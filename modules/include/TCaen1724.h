/////////////////////////////////////////////////////
// 
// Caen 1724 8 ch 100 MHz FADC
//
// Magnus Lundin and Pavrel 07-06-12
//
////////////////////////////////////////////////////
#ifndef _TCaen1724_H_
#define _TCaen1724_H_

#include "TObject.h"


class TCaen1724: public TObject{

private:

  int ffd;                               // File desc. 
  volatile void *fbase;                  // Base address for io
  volatile UShort_t *fbase16;            // 16 bit access
  volatile UInt_t   *fbase32;            // 32 bit access
  UInt_t hwaddress;                      // The hwaddress
  UShort_t debug;                        // Debug flag

public:
  TCaen1724( UInt_t BASE, UShort_t debugflag = 0);  // hw address 
  virtual ~TCaen1724();

  // Generic write/read access to the 16 bit mem area
  void SetShort( UShort_t address, UShort_t value);
  UShort_t GetShort( UShort_t address);
  UInt_t GetLong( UShort_t address);
  void SetLong( UShort_t address, UInt_t value);

  void Start();
  void Stop();
  void SwTrigg(){ SetLong( 0x8108, 0x1); return;};
  void SwReset(){ SetLong( 0xEF24, 0x1); return;};
  void SwClear(){ SetLong( 0xEF28, 0x1); return;};
  void TestWaveForm(){ SetLong( 0x8000, (0x0008 | GetLong( 0x8000))); return;};

  UShort_t BufferFull(){ return (0x0001 & GetLong(0x8104) >> 4);};
  UShort_t EventReady(){ return (0x0001 & GetLong(0x8104) >> 3);};
  UShort_t Running(){ return (0x0001 & GetLong(0x8104) >> 2);};

  UShort_t GetData( UShort_t *val);

  void Init( UInt_t BASE);

  // debug functions
  void Info();               
  void bitprint( UInt_t buffer);
  ClassDef( TCaen1724, 0)
};

#endif /* _TCaen1724_H_ */




