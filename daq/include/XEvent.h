#ifndef ROOT_XEvent
#define ROOT_XEvent

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// XEvent                                                               //
//                                                                      //
// Description of a X event                                             //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


#include "TObject.h"
#include "TClonesArray.h"

enum { ENVqdc = 32 };

class TDirectory;

//////////////////////////////////////////////////////////////////
// The tagger tdc class
class TTagg : public TObject
{
  private:

    Short_t fIndex;    // The energy index                              
    Short_t fData;     // The TDC value

  public:

    TTagg() {}
    TTagg(UShort_t i, UShort_t d ): fIndex(i), fData(d) { }
    virtual ~TTagg() { }

    UShort_t Index() {return fIndex;}
    UShort_t Data() {return fData;}

    ClassDef( TTagg, 1)
};
//////////////////////////////////////////////////////////////////
// The tagger tdc class (multi)
class TVtdc : public TObject
{
  private:

    Short_t fIndex;    // The energy index                              
    Short_t fData;     // The TDC value

  public:

    TVtdc() {}
    TVtdc(UShort_t i, UShort_t d ): fIndex(i), fData(d) { }
    virtual ~TVtdc() { }

    UShort_t Index() {return fIndex;}
    UShort_t Data() {return fData;}

    ClassDef( TVtdc, 1)
};
//////////////////////////////////////////////////////////////////
// The vme qdc class
class TVqdc : public TObject
{
  private:

    UShort_t fIndex;    // The channel number
    UShort_t fData;     // The adc value

  public:

    TVqdc() {}
    TVqdc(UShort_t i, UShort_t d ): fIndex(i), fData(d) { }
    virtual ~TVqdc() { }

    UShort_t Index() {return fIndex;}
    UShort_t Data() {return fData;}

    ClassDef( TVqdc, 1)
};


//////////////////////////////////////////////////////////////////
// The xevent structure
class XEvent : public TObject
{

  private:

    UInt_t fEventNo;              // event number
    
    UShort_t     fNVtdc;          // # multi tagger tdc channels in current event
    TClonesArray *fVtdc;          // Pointer to TClonesArray 
    static TClonesArray *fgVtdc;  // The start of TCA 192 long

    UShort_t     fNVqdc;          // Number of qdc channels in current event
    TClonesArray *fVqdc;          // Pointer to TClonesArray 
    static TClonesArray *fgVqdc;  // The start of TCA 64 long

   
    // crate 1
    UInt_t fc1_1[12];            // 2259 VDC
    UInt_t fc1_2[12];            // 2259 VDC
    UInt_t fc1_3[12];            // 2259 VDC
    UInt_t fc1_4[8];             // 2228 TDC
    UInt_t fc1_5[8];             // 2228 TDC
    UInt_t fc1_6[8];             // 2228 TDC
    UInt_t fc1_7[8];             // 2228 TDC
    UInt_t fc1_9[8];             // 4208 TDC

  public:

    XEvent();
    virtual ~XEvent();

    void SetEventNumber(UInt_t n) { fEventNo = n; }
    UInt_t GetEventNumber() { return fEventNo; }
    
    void AddVtdc(UShort_t i, UShort_t d);              // Add TVtdc obj to TCA
    TClonesArray *GetVtdc() const { return fVtdc; }  
    UShort_t GetNVtdc(){return fNVtdc;}

    void AddVqdc(UShort_t i, UShort_t d);              // Add TVqdc obj to TCA
    TClonesArray *GetVqdc() const { return fVqdc; }  
    UShort_t GetNVqdc(){return fNVqdc;}

    
    void  Setc1_1(UShort_t ch, UShort_t data) { fc1_1[ch] = data; }
    UShort_t Getc1_1(UShort_t ch) { return fc1_1[ch]; }
    void  Setc1_2(UShort_t ch, UShort_t data) { fc1_2[ch] = data; }
    UShort_t Getc1_2(UShort_t ch) { return fc1_2[ch]; }
    void  Setc1_3(UShort_t ch, UShort_t data) { fc1_3[ch] = data; }
    UShort_t Getc1_3(UShort_t ch) { return fc1_3[ch]; }

    void  Setc1_4(UShort_t ch, UInt_t data) { fc1_4[ch] = data; }
    UInt_t Getc1_4(UShort_t ch) { return fc1_4[ch]; }
    void  Setc1_5(UShort_t ch, UInt_t data) { fc1_5[ch] = data; }
    UInt_t Getc1_5(UShort_t ch) { return fc1_5[ch]; }
    void  Setc1_6(UShort_t ch, UInt_t data) { fc1_6[ch] = data; }
    UInt_t Getc1_6(UShort_t ch) { return fc1_6[ch]; }
    void  Setc1_7(UShort_t ch, UInt_t data) { fc1_7[ch] = data; }
    UInt_t Getc1_7(UShort_t ch) { return fc1_7[ch]; }
    void  Setc1_9(UShort_t ch, UInt_t data) { fc1_9[ch] = data; }
    UInt_t Getc1_9(UShort_t ch) { return fc1_9[ch]; }

    void Clear();
    void PrintAll();

    ClassDef(XEvent, 1)  // XEvent structure 
};

#endif
