////////////////////////////////////////////////////////////////////////

#include "TDirectory.h"

#include "ScalerEvent.h"

ClassImp(ScalerEvent)

//________________________________________________________________________
ScalerEvent::ScalerEvent()
{}
//________________________________________________________________________
ScalerEvent::~ScalerEvent()
{}
//________________________________________________________________________
void ScalerEvent::PrintScalers()
{
  UShort_t i;

  printf("scaler event No. %d\n", GetEventNumber());
  
  printf("---- vme scaler ----\n");
  for( i = 0; i < 32; i++)
    printf("ch %d \t %u\n", i, Getvs0(i));
  printf("---- vme scaler ----\n");
  for( i = 0; i < 32; i++)
    printf("ch %d \t %u\n", i, Getvs1(i));
  printf("---- camac scaler crate 1 slot 9 ----\n");
  for( i = 0; i < 32; i++)
    printf("ch %d \t %ul\n", i, Getc1_8(i));

}
//________________________________________________________________________
void ScalerEvent::ClearScalers()
{
  int i;
  
  for ( i = 0; i < 32; i++) 
    {
      Setvs0( i, 0);
      Setvs1( i, 0);
    }
  for ( i = 0; i < 32; i++)
    Setc1_8( i, 0);
}

