// John Annand   19th Feb 2014  Modify for SIS1100

#include "TLRS2277.h"

ClassImp( TLRS2277 )

TLRS2277::TLRS2277(TCamacBranch *cbd, int c, int n, int ch): 
fCBD( cbd ), fCrate(c), fSlot(n)
{
  if( ! cbd->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fNoChannels = ch;

  fClear = cbd->cnaf( fCrate, fSlot, 0, 9);
  fTest = cbd->cnaf( fCrate, fSlot, 0, 25);

  fReadStatus = cbd->cnaf( fCrate, fSlot, 0, 1);
  fWriteStatus = cbd->cnaf( fCrate, fSlot, 0, 17);

  pRead = cbd->cnaf( fCrate, fSlot, 0, 0);

  fClearLam = cbd->cnaf( fCrate, fSlot, 0, 10);
  fDisableLam = cbd->cnaf( fCrate, fSlot, 0, 24);
  fEnableLam = cbd->cnaf( fCrate, fSlot, 0, 26);
  fBip = cbd->cnaf( fCrate, fSlot, 0, 27);
  fTestBusy = cbd->cnaf( fCrate, fSlot, 1, 27);

  Init();
}

TLRS2277::TLRS2277(TCamacCrate *C, int n, int ch):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{
  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fNoChannels = ch;

  fClear = fCBD->cnaf( fCrate, fSlot, 0, 9);
  fTest = fCBD->cnaf( fCrate, fSlot, 0, 25);

  fReadStatus = fCBD->cnaf( fCrate, fSlot, 0, 1);
  fWriteStatus = fCBD->cnaf( fCrate, fSlot, 0, 17);

  pRead = fCBD->cnaf( fCrate, fSlot, 0, 0);

  fClearLam = fCBD->cnaf( fCrate, fSlot, 0, 10);
  fDisableLam = fCBD->cnaf( fCrate, fSlot, 0, 24);
  fEnableLam = fCBD->cnaf( fCrate, fSlot, 0, 26);
  fBip = fCBD->cnaf( fCrate, fSlot, 0, 27);
  fTestBusy = fCBD->cnaf( fCrate, fSlot, 1, 27);

  Init();
}

void TLRS2277::Init()
{
  SetStatus( 0xffff);
  UnSetStatusBit(11);  // Disable "hit trailing edge recording"
  UnSetStatusBit(10);  // Disable even channels test input
  UnSetStatusBit(9);   // Disable odd channels test input
  Clear();
  DisableLam();
  ClearLam();
}

TLRS2277::~TLRS2277( )
{
}


void TLRS2277::SetStatusBit( UShort_t bit)
{
  // 1st bit is No 1
  UShort_t rstat = fCBD->Read(fReadStatus);
  fCBD->Write(fWriteStatus, rstat | (1 << (bit - 1)) );
 
  return;
}


void TLRS2277::UnSetStatusBit( UShort_t bit)
{
  // 1st bit is No 1
  UShort_t rstat = fCBD->Read(fReadStatus);
  fCBD->Write(fWriteStatus, rstat & (0xffff & ~(1 << (bit - 1))));
 
  return;
}


Int_t TLRS2277::GetData( UShort_t *ch, UShort_t *val)
{
  int i;
  for( i = 0; (i < fNoChannels) && ( Q() == 1); i++)
    {
      *ch++ = 0x1f & fCBD->Read(pRead-2) >> 1; 
      *val++  = fCBD->Read(pRead);      
    }
  Clear();
  return i;
}

UShort_t TLRS2277::operator[](int n )
{
  return fCBD->Read(pRead);
}

void TLRS2277::Test(int l)
{
  volatile UShort_t dum;

  dum = fCBD->Read(fTest);
}

void TLRS2277::Print( )
{
  UShort_t mult, ch[fNoChannels], val[fNoChannels];

  mult = GetData( ch, val);

  for( int i = 0; i < mult; i++)
    printf( "Channel: %d, value: %d\n", ch[i], val[i]);
}

void TLRS2277::bitprint( UShort_t buffer)
{
  int i = 15;
  while( i >= 0)
    {
      if ( (buffer >> i) & 0x0001 )
        printf("1");
      else
        printf("0");
      if ( (i % 8 == 0) && (i != 0))
        printf("|");
      i--;
    }
  printf("xxxx\n");
}
