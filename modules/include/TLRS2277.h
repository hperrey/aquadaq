/////////////////////////////////////////////////////
// 
// LeCroy 2277 multi hit tdc
//
// Magnus Lundin 061026
// John Annand   19th Feb 2014  Modify for SIS1100
//
////////////////////////////////////////////////////
#ifndef _TLRS2277_H_
#define _TLRS2277_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

class TLRS2277: public TObject{

private:

  TCamacBranch *fCBD;
  const int fCrate;             // Crate number
  const int fSlot;              // Slot number
  int fNoChannels;              // Number of channels

  UInt_t pRead;
  
  UInt_t fClear; // Clear module
  UInt_t fTest;  // Test module

  UInt_t fReadStatus;     // Read the status register
  UInt_t fWriteStatus;    // Write to the status register

  UInt_t fClearLam;
  UInt_t fDisableLam;
  UInt_t fEnableLam;
  UInt_t fBip;            // Buffering in progress
  UInt_t fTestBusy;       // Busy doing test

public:
  TLRS2277(TCamacBranch *cbd, int c, int n, int ch = 32);
  TLRS2277(TCamacCrate *C, int n, int ch = 32);
  virtual ~TLRS2277();

  UShort_t operator[](int);
  Int_t GetData( UShort_t *ch, UShort_t *val);

  Bool_t Q() {return fCBD->Q();} // Q response
  Bool_t X() {return fCBD->X();} // X response


  void Init();
  void SetStatusBit( UShort_t bit);
  void UnSetStatusBit( UShort_t bit);
  void SetStatus(UShort_t bp){ fCBD->Write(fWriteStatus,bp);}
  void PrintStatus(){ printf("Status 0x%x\n", fCBD->Read(fReadStatus));}

  Bool_t ClearLam(){ fCBD->Read(fClearLam); return Q();}
  Bool_t DisableLam(){ fCBD->Read(fDisableLam); return Q();}
  Bool_t EnableLam(){ fCBD->Read(fEnableLam); return Q();}
  Bool_t Bip(){ fCBD->Read(fBip); return Q();}
  Bool_t TestBusy(){ fCBD->Read(fTestBusy); return Q();}

  void SetCommonStart(){ SetStatusBit(10);};
  void SetCommonStop(){ UnSetStatusBit(10);};
  void Clear() {fCBD->Read(fClear);}
  void Test(int l=3);
  void Print();
  void bitprint( UShort_t buffer);
  
  ClassDef( TLRS2277, 0)  // Camac unit
};

#endif /* _TLRS2277_H_ */
