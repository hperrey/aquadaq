// John Annand  19th Feb 2014  Modify for SIS1100

#include "TLRS4208.h"

ClassImp( TLRS4208 )

TLRS4208::TLRS4208(TCamacBranch *cbd, int c, int n): 
fCBD( cbd ), fCrate(c), fSlot(n)
{
  InitUnit();
}

TLRS4208::TLRS4208(TCamacCrate *C, int n):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{
  InitUnit();
}

TLRS4208::~TLRS4208( )
{
}

void TLRS4208::InitUnit()
{
  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fClear = fCBD->cnaf( fCrate, fSlot, 0, 9);
  fTest = fCBD->cnaf( fCrate, fSlot, 0, 25);

  for( int i = 0; i < fNoChannels ; i++)
    {
      pRead[i] = fCBD->cnaf( fCrate, fSlot, i, 2);
      pReadH[i] = fCBD->cnaf( fCrate, fSlot, i, 2, 0);
      pWrite[i] = fCBD->cnaf( fCrate, fSlot, i, 17);
    }
}

Int_t TLRS4208::ReadArray( UInt_t *Array, Int_t count)
{
  // Read an array of count values into Array
  // from channel starting on 0.
  // 24 bit read done in 2 chunks: 8 msb, followed by 16 lsb
  // Read is done F2, clears on last channel, count=7
  
  int i;
  UInt_t lsb,msb;
  for( i = 0; i < count; i++){
    //*Array++ = Read(i);
    msb = (UInt_t)fCBD->Read(pReadH[i]);
    lsb = (UInt_t)fCBD->Read(pRead[i]);
    Array[i] = (lsb & 0xffff) | ((msb & 0xff) << 16);
  }
  //fCBD->Read(fClear);
  return i;
}

void TLRS4208::Test(int l)
{
  volatile Short_t dum;

  dum = fCBD->Read(fTest);
}

void TLRS4208::Print( )
{
  for( int i = 0; i < fNoChannels; i++)
    printf( "Channel: %d, value: %d\n", i, fCBD->Read(pRead[i]));
}
