//////////////////////////////////////////////////////////////////////
//
// Implementation of the SAL 16-channel scaler 
//
//////////////////////////////////////////////////////////////////////
#include "TSAL4417.h"
#include <stdlib.h>

ClassImp( TSAL4417);

TSAL4417::TSAL4417(TCamacCrate *C, int n, int channels):
  fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n), fChannels(channels)
{
 
  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  pClear = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 0, 9, 1);  // T = 1 

  // Pointer to sequential reading, do a 16 bit access
  pReadClear = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 0, 2, 1);  // T = 1 

  // Pointer to sequential reading, do a 16 bit access
  pRead      = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 0, 0, 1);  // T = 1 

}

TSAL4417::~TSAL4417( )
{

}

UShort_t TSAL4417::ReadClearArray( UInt_t *arr)
{

  for( int i = 0; i < fChannels; i++)
    {
      *arr    = *( volatile UShort_t *)(pReadClear  - 1) << 16; 
      *arr++ |= *( volatile UShort_t *)pReadClear;
    }
  return fChannels;
}                        

UShort_t TSAL4417::ReadArray( UInt_t *arr)
{

  for( int i = 0; i < fChannels; i++)
    {
      //      *pComm = (0x218 << 4) + i; 

      *arr    = *( volatile UShort_t *)(pRead  - 1) << 16; 
      *arr++ |= *( volatile UShort_t *)pRead;
    }
  return fChannels;
}                        

void TSAL4417::PrintAll()
{

  UInt_t testArray[fChannels];

  ReadClearArray( testArray );   

  for ( int i = 0; i < fChannels; i++)
      printf("Channel %d \t 0x%08x \n", i, testArray[i] );
  
}


