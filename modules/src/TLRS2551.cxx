//  John Annand 20th Feb. 2014  Modified for SIS1100
#include "TLRS2551.h"
#include <math.h>

ClassImp( TLRS2551);

TLRS2551::TLRS2551(TCamacBranch *cbd, int c, int n, int ch ):
fCBD( cbd ), fCrate(c), fSlot(n)
{
 
  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fChannels = ch;

  for( int i = 0; i < ch; i++)  // 16 bit access
    {
      pRead[i] = fCBD->cnaf( fCrate, fSlot, i, 0, 1);
      pReadClear[i] = fCBD->cnaf( fCrate, fSlot, i, 2, 1);
    }

  fClear = fCBD->cnaf( fCrate, fSlot, 0, 9);
  fIncrement = fCBD->cnaf( fCrate, fSlot, 0, 25);
}

TLRS2551::TLRS2551(TCamacCrate *C, int n, int ch ):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{
 
  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fChannels = ch;

  for( int i = 0; i < ch; i++) // 16 bit access
    {
      pRead[i] = fCBD->cnaf( fCrate, fSlot, i, 0, 1);
      pReadClear[i] = fCBD->cnaf( fCrate, fSlot, i, 2, 1);
    }

  fClear = fCBD->cnaf( fCrate, fSlot, 0, 9);
  fIncrement = fCBD->cnaf( fCrate, fSlot, 0, 25);
}

TLRS2551::~TLRS2551( )
{

}

UShort_t TLRS2551::ReadArray( UInt_t *arr)
{
  for( int i = 0; i < fChannels; i++)
    {
      // 24 bit access
      *arr    = fCBD->Read(pRead[i]-2) << 16; 
      *arr++ |= fCBD->Read(pRead[i]);
    }
  return fChannels;
}

UShort_t TLRS2551::ReadClearArray( UInt_t *arr)
{
  for( int i = 0; i < fChannels; i++)
    {
      // 24 bit access
      *arr    = fCBD->Read(pReadClear[i]-2) << 16; 
      *arr++ |= fCBD->Read(pReadClear[i]);
    }
  return fChannels;
}

void TLRS2551::PrintAll()
{

  UInt_t testArray[fChannels];

  ReadArray( testArray );   

  for ( int i = 0; i < fChannels; i++)
      printf("Channel %d \t 0x%08x \n", i, testArray[i] );
  
}


// 
// Function that tests the camac communication. No inhibit is needed if 
// channel ch has no input
//
// highestBit is the highest bit for which the test is carried out
//
// ch is the channel that will be tested
//
// bp is the bit pattern of faulty bits, ie if bit 2 and 7 is known to be faulty
// this is masked by setting bp = 0x000042
//
UShort_t TLRS2551::TestBits( UShort_t highestBit, UShort_t ch, UInt_t bp)
{
  UShort_t bit;
  UInt_t i = 0;
  UInt_t values[12];

  // Clear all channels
  Clear();

  for ( bit = 0; bit <= highestBit; bit++)
    {
      // fprintf( stderr, "bit %d\n", bit);
      //      fprintf( stderr, "bit %d %d\n", bit, (UInt_t) pow(2,bit));
      while ( (UInt_t )pow(2,bit) > i)
	{
	  //fprintf( stderr, "%d,", i);
	  i++;
	  Increment();
	}

      ReadArray( values);
      if ( (values[ch] | bp) != (i | bp) )  // mask the faulty bits
	  {
	    printf("error ch %d loop %d\n", ch, i);
	    PrintAll();
	    return 1;
	  }
    }
  return 0;      
}
