////////////////////////////////////////////////////////////////////////////
// 
// Caen C207 (CAMAC)
// 16 channel programmable discriminator
// 
// Sets the thresholds in steps of 2 mV, ie a threshold setting of 0x0001
// corresponds to a threshold of 2 mV. Should be above 6 mV for proper 
// functioning.
//
///////////////////////////////////////////////////////////////////////////////////
#ifndef _TCaenC207_H_
#define _TCaenC207_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

class TCaenC207: public TObject{

private:

  static const int fNoChannels = 16;

  TCamacBranch *fCBD;           
  Int_t fCrate;			// Crate number
  Int_t fSlot;			// Slot number

  volatile UShort_t *fReset;	          // Reset module
  volatile UShort_t *fEnableChannels;	  // Enable channels
  volatile UShort_t *fTestOutputs;        // Generate pulses on the outputs
  volatile UShort_t *pRead[fNoChannels];  // Read addresses
  volatile UShort_t *pWrite[fNoChannels]; // Write addresses
  
  void InitUnit();		

  volatile Short_t *func(int func, int address) // Get address
    {return (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, address, func); }

public:

  TCaenC207(TCamacBranch *cbd, int c, int n);
  TCaenC207(TCamacCrate *C, int n);
  virtual ~TCaenC207();

  void Reset(){ if (*fReset);}  
  void EnableChannels( UShort_t bitp);
  void SetThreshold( UShort_t ch, UShort_t val);
  void SetThresholds( UShort_t val);
  void PrintThresholds();
  void TestOutputs(){ if (*fTestOutputs);}

  Bool_t Q() {return fCBD->Q();} // Q response
  Bool_t X() {return fCBD->X();} // X response

  ClassDef( TCaenC207, 0 )  
};

#endif // _TCaenC207_HH_
