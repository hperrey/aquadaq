/////////////////////////////////////////////////////
// 
// Caen 775 32 Channel TDC
//
// Magnus Lundin 05-07-27 
//
////////////////////////////////////////////////////
#ifndef _TCaen775_H_
#define _TCaen775_H_

#include "TObject.h"


class TCaen775: public TObject{

private:

  int ffd3216;                           // File desc. 
  int ffd3232;                           // File desc. 
  volatile void *ftmpbase3232;           // Base address for io
  volatile void *ftmpbase3216;           // Base address for io
  volatile UShort_t *fbase3216;          // 16 bit access
  volatile UInt_t   *fbase3232;          // 32 bit access
  UInt_t hwaddress;                      // The hwaddress
  UShort_t debug;                        // Debug flag

public:
  TCaen775( UInt_t BASE, UShort_t threshold, UShort_t debugflag = 0);  // hw address and thresholds
  virtual ~TCaen775();

  // Generic write/read access to the 16 bit mem area
  void SetShort( UShort_t address, UShort_t value);
  UShort_t GetShort( UShort_t address);

  void Init( UInt_t BASE, UShort_t threshold);
  void DataReset() { SetShort(0x1032,0x4); SetShort(0x1034,0x4);}
  void SoftwareReset() { SetShort(0x1006,0x80); SetShort(0x1008,0x80);}

  void SetThreshold( UShort_t channel, UShort_t value);
  void SetThresholds( UShort_t value);  
  void DeactivateChannels( UInt_t bitmask);  // mask off channels according to the bitpattern

  // Set the TDC full range, from 140 to 1200 ns, see manual for valid value    
  void SetRange( UShort_t value);  
  UShort_t GetRange(); 

  void IncludeOverflows(){ SetShort( 0x1032, 0x8);};
  void ExcludeOverflows(){ SetShort( 0x1034, 0x8);};

  void IncludeUnderThreshold(){ SetShort( 0x1032, 0x10);};
  void ExcludeUnderThreshold(){ SetShort( 0x1034, 0x10);};

  UShort_t DataReady(){ if ( (GetShort( 0x100e) & 0x0001) == 0x0001 ) return 1; else return 0;}
  UShort_t Busy(){ if ( (GetShort( 0x100e) & 0x0004) == 0x0004 ) return 1; else return 0;}
  UShort_t BufferEmpty();                             // is the data buffer empty
  UShort_t BufferFull();                              // is the data buffer full
  UShort_t GetMultiplicity( UInt_t buffer);           // the multiplicity of the event
  void GetDatum( UShort_t *channel, UShort_t *value); // get the next datum (ch,val) of the event
  UShort_t IsEOB( UInt_t buffer);                     // have we reached the End Of Block of 
                                                      // of the current event

  UShort_t GetData( UShort_t *ch, UShort_t *val);                                         

  // debug functions
  void PrintThresholds();     
  void Info();               
  void PrintStatus();
  void bitprint( UInt_t buffer);
  ClassDef( TCaen775, 0)
};

#endif /* _TCaen775_H_ */




