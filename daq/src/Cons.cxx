/*
  Consumer process

  At run start:

  Start the data reader task.
  Tell the producer daemon at the VME system to load the data writer task.
  Tell the producer daemon to start the run.
    The data writer connects to the data reader task
    and starts to write data.

  At run stop:
  Tell the producer daemon to stop the run.
    The data writer writes end of run to the data reader task
    The data reader terminates.
  
*/

#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sstream>

#include "TROOT.h"

#include <DAQ.h>
#include <Mmap.h>

#include <configVariables.h> // parameters generated during CMake config step

int StartDaq(int events);

// initializer for GUI needed for interactive interface
extern void  InitGui();
VoidFuncPtr_t initfuncs[] = { InitGui, 0 };

TROOT root("Rint","The ROOT Interactive Interface", initfuncs);

//______________________________________________________________________________
int main(int argc, char **argv)
{
  char str[218];
  struct run *Run;
  int wstat;
  int runno = 0;
  
  Run = InitMem(1);

  FILE *fp = fopen(".runno", "r+");
  if( fp )
    {
      fscanf( fp, "%d", &runno);
      runno++;
      rewind(fp);
      fprintf( fp, "%d", runno);
      fclose(fp);
    }
  else
    runno = 999;
  
  Run->RunNumber = runno;
  Run->DoWrite = 1;
  Run->DoFill = 1;

  StartDaq(12);

  return 0;
}


int StartDaq(int events)
{
  
  switch( fork() )
    {
    case -1:
      perror("Fork");
      break;
    case 0:
      std::stringstream ss;
      ss << PACKAGE_INSTALL_DIR << "/bin/aquadaq";
      execl( ss.str().c_str(), "aquadaq", NULL);
    }
  return 0;
}
