/////////////////////////////////////////////////////
// 
// Caen 775 32 Channel TDC
//
// Magnus Lundin 05-07-27 
//
// 2010-06-10 Changed data readout mask to 0x7fff
//            Changed handling of under and overflows
//            Changed ch No readout mask to 0x1f
//            ctor now leaves module without under and overflow 
//            suppression. The user must explicitly request this 
//            using the SuppressUnderflow() and SuppressOverflow() 
//            functions.
//
// 2010-11-11 Modified the above entry
// John Annand 19/02/14 Modify for TSIS1100 interface class
//
////////////////////////////////////////////////////
#ifndef _TCaen775_H_
#define _TCaen775_H_

#include "TObject.h"

class TSIS1100;
class TCaen775: public TObject{

private:
  TSIS1100* fSIS;
  UInt_t fbase;                  // Base address for io
  UInt_t hwaddress;                      // The hwaddress
  UShort_t debug;                        // Debug flag

public:
  TCaen775( TSIS1100*, UInt_t, UShort_t = 0);  // hw address and thresholds
  virtual ~TCaen775();

  // Generic write/read access to the 16 bit mem area
  void SetShort( UShort_t address, UShort_t value);
  UShort_t GetShort( UShort_t address);
  void DataReset() { SetShort(0x1032,0x4); SetShort(0x1034,0x4);}
  void SoftwareReset() { SetShort(0x1006,0x80); SetShort(0x1008,0x80);}
  void SuppressUnderflow( Bool_t flag, UShort_t thres = 16);
  void SuppressOverflow( Bool_t flag);
  void SetUnderflowThreshold( UShort_t ch, UShort_t thres);
  void SetUnderflowThresholds( UShort_t thres);
  void DeactivateChannels( UInt_t bitmask);  // mask off channels according to the bitpattern

  //Set the TDC full range, from 140 to 1200 ns, see manual for valid value    
  void SetRange( UShort_t value);  
  UShort_t GetRange(); 

  UShort_t DataReady(){ if ( (GetShort( 0x100e) & 0x0001) == 0x0001 ) return 1; else return 0;}
  UShort_t Busy(){ if ( (GetShort( 0x100e) & 0x0004) == 0x0004 ) return 1; else return 0;}
  UShort_t BufferEmpty();                             // is the data buffer empty
  UShort_t BufferFull();                              // is the data buffer full
  UShort_t GetMultiplicity( UInt_t buffer);           // the multiplicity of the event
  void GetDatum( UShort_t *channel, UShort_t *value); // get the next datum (ch,val) of the event
  UShort_t IsEOB( UInt_t buffer);                     // have we reached the End Of Block of 
                                                      // of the current event
  UShort_t GetData( UShort_t *ch, UShort_t *val);                                         

  // debug functions
  void PrintThresholds();     
  void Info();               
  void PrintStatus();
  void bitprint( UInt_t buffer);
  ClassDef( TCaen775, 0)
};

#endif /* _TCaen775_H_ */




