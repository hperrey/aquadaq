#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#include "TMapFile.h"
#include "TStopwatch.h"
#include "TObjString.h"
#include <TTimeStamp.h>
#include "TFile.h"
#include "TTree.h"
#include <sstream>

#include "Mmap.h"

#include "XEvent.h"
#include "ScalerEvent.h"

#include "XHist.h"
#include "ScalerHist.h"

#include "log.h"

#include "Experiment.h"
#include <configVariables.h>
#include <iostream>

#define HISTMAPFILE DATA_PATH "/histos.map"
#define MEG     1024*1024
#define HISTMAPSIZE 64*MEG // This needs to large enough or bad things will happen.

static int FileExists(const char *fname);
static TMapFile *FindMapFile(const char *fname, const int size, int recreate);

using namespace aquadaq;

int main()
{

  LOG(logINFO) << PACKAGE_STRING << " starting up!";
  LOG(logINFO) << "***** Welcome *******";
  char str[218];
  struct run *runmap;
  XEvent *xevent;
  ScalerEvent *scalerevent;
  XHist *xhist;
  ScalerHist *scalerhist;
  TStopwatch timer;
  TMapFile *mfile;
  TFile *hfile = 0;
  TTree *xtree = 0;
  TTree *stree = 0;

  runmap = InitMem(0);

  Int_t comp   = 1;	
  Int_t split  = 1;
  Int_t write  = runmap->DoWrite;
  Int_t hfill  = runmap->DoFill;


  TROOT simple("simple", runmap->Experiment );


  // Find the correct address of the memory mapped file

  // Do this in rootlogon.C instead (producer is larger)
  // TMapFile *tmp;
  // tmp = TMapFile::Create("dummy.map", "recreate", 16 * 1024 * 1024);
  // tmp->Print();
  // exit(0);  

  // Create the histogram map file 
  mfile = FindMapFile( HISTMAPFILE, HISTMAPSIZE, 1);
  mfile->Update();

  printf("mmapped file done\n");

  // Create the histograms. These are automagically added to the map file 
  xhist = new XHist();
  xhist->CreateHistograms();   
  scalerhist = new ScalerHist();
  scalerhist->CreateHistograms();

  mfile->Update();

  sprintf( str, "%ld", runmap->RunNumber);
  TObjString *runn = new TObjString( str ); // run number
  TObjString *rune = new TObjString( runmap->Experiment ); // experiment description
  TObjString *runv = new TObjString( PACKAGE_STRING ); // AQUADAQ version

  if( write )
    {
      if( ! runmap->fPrefix[0] ){
	std::stringstream ss;
	ss << DATA_PATH << "/Data";	
	sprintf( str, "%s%03ld.root", ss.str().c_str(),runmap->RunNumber );
      } else {
        sprintf( str, "%s%03ld.root", runmap->fPrefix, runmap->RunNumber );
      }

      // Provide some error recovery...
      // need to fix the case where all files already exists...
      for( int i = 'a';  FileExists( str ) && i < 'z'; i++){
	if( ! runmap->fPrefix[0] ){
	  std::stringstream ss;
	  ss << DATA_PATH << "/Data";	
	  sprintf( str, "%s%03ld%c.root", ss.str().c_str(),runmap->RunNumber, i );
	}else{
	  sprintf( str, "%s%03ld%c.root", runmap->fPrefix, runmap->RunNumber, i );
	}
      }

      // set up log file
      FILE* lfile;
      std::string logfile(str);
      logfile = logfile.substr(0,logfile.find_last_of(".")); //strip .root file extension
      logfile += ".log";
      lfile = fopen(logfile.c_str(), "a");
      SetLogOutput::Stream() = lfile;
      SetLogOutput::Duplicate() = true;
      
      LOG(logINFO) << "Writing data to file: " << str;

      hfile = new TFile( str, "RECREATE", runmap->Experiment);
      hfile->SetCompressionLevel(comp);

      runn->Write("RunNumber");
      rune->Write("Experiment");
      runv->Write("DAQVersion");
    }

  mfile->Add( runn, "RunNumber");
  mfile->Add( rune, "Experiment");
  mfile->Update();

  LOG(logINFO) << PACKAGE_STRING << " starting run!";

  // Create one event
  xevent = new XEvent();
  scalerevent = new ScalerEvent();
  TTimeStamp* timestamp = new TTimeStamp(); // timestamp for scaler events linking scalers to computer clk
  Long64_t globEvtno = 0; // global event number to link entries in different trees created below

  if ( write)
    { 
      // Create a ROOT Tree and one superbranch
      xtree = new TTree("TX", "X-Tree");
      xtree->SetAutoSave(10000000);  // autosave when 0.01 Gbyte written
      Int_t bufsize = 256000;
      if (split)  bufsize /= 4;      
      xtree->Branch("xbranch", "XEvent", &xevent, bufsize, split);
      xtree->Branch("globaleventno", "Long64_t", &globEvtno, bufsize, split);

      // Create a ROOT Tree and one superbranch
      stree = new TTree("TS", "Scaler-Tree");
      stree->SetAutoSave(10000000);  // autosave when 0.01 Gbyte written
      bufsize = 25600;
      if (split)  bufsize /= 4;
      stree->Branch("scalerbranch", "ScalerEvent", &scalerevent, bufsize/4, split);
      stree->Branch("globaleventno", "Long64_t", &globEvtno, bufsize, split);
      stree->Branch("timestamp", "TTimeStamp", &timestamp, bufsize, split);
    }
  
  Int_t events = 0, sevents = 0, bytes = 0;
  
  //
  // Create an experiment object
  //
  fprintf( stderr, "Creating experiment\n");
  Experiment *exp = new Experiment();


  ////////////////////////////////////////////////////////////////////////////
  // THE loop
  //

  Print( runmap);

  // Create a timer object to benchmark this loop
  double lastScalerEvent = 0;
  double lastHistUpdate  = 0;
  timer.Start( kTRUE); // Start watch at 0

  while ( runmap->RunState == trsRunning)
    {
      // Scaler event
      if ( (timer.RealTime() - lastScalerEvent) > 2.0 )
	{ 
	  if ( exp->ReadScalerEvent( scalerevent) > 0)
	    {
	      globEvtno++; // increase global event number
	      timestamp->Set(); // set timestamp to current time
	      if ( write) bytes += stree->Fill();  
	      if ( hfill) scalerhist->FillHistograms( scalerevent);
	      //	      scalerevent->PrintScalers();
	      scalerevent->ClearScalers();
	    }
	  sevents++;
	  lastScalerEvent = timer.RealTime();
	}
      timer.Continue();
 
      // Experimental event 
      if ( exp->ReadXEvent( xevent) > 0)
	{
	  globEvtno++; // increase global event number
	  if ( write) bytes += xtree->Fill();  	      
	  if ( hfill) xhist->FillHistograms( xevent);		  
	  events++;
	  //xevent->PrintAll();
	  xevent->Clear();
	}
      
      // Update histograms
      if ( ((events % 1000) == 0 && events) || ((timer.RealTime() - lastHistUpdate) > 5.0) )
	{
	  lastHistUpdate = timer.RealTime();
          if( runmap->HistReset)
            {
              xhist->ResetAll();
	      scalerhist->ResetAll();
              runmap->HistReset = 0;
            }

 	  mfile->Update();   
          runmap->nEvents = events;
	  LOG(logINFO) << "Got " << events << " events and "<< sevents << " scaler events...";
	}      
      timer.Continue();
    }
  ////////////////////////////////////////////////////////////////////////////


  delete timestamp;
  LOG(logDEBUGMAIN) << "deleting event";
  delete xevent;
  LOG(logDEBUGMAIN) << "deleting scalerevent";
  delete scalerevent;
  LOG(logDEBUGMAIN) << "deleting exp";
  delete exp;

  timer.Stop();
  runmap->nEvents = events;

  if ( write) 
    {
      if( hfill) 
	{
	  xhist->WriteAll(); 
	  scalerhist->WriteAll();
	}
      hfile->Write();
      //stree->Print();
      //xtree->Print();
      hfile->Close();
    }

  mfile->Update();       // updates all objects in shared memory
  mfile->Close();


  //  Stop timer and print results
  Float_t mbytes = 0.000001*bytes;
  Double_t rtime = timer.RealTime();
  Double_t ctime = timer.CpuTime();

  LOG(logINFO) << std::endl << PACKAGE_STRING << " shutting down:";
  LOG(logINFO) << events << " events and " << bytes << " bytes processed.";
  LOG(logINFO) << "RealTime=" << rtime << " seconds, CpuTime=" << ctime << " seconds";
  LOG(logINFO) << std::fixed << std::setprecision( 2 ) << events/rtime << " Events per second.";
  LOG(logINFO) << "compression level=" << comp << ", split=" << split;
  LOG(logINFO) << "You write "<< mbytes/rtime<< " Mbytes/Realtime seconds";
  LOG(logINFO) << "You write "<< mbytes/ctime<<" Mbytes/Cputime seconds";

  UnMem();
  
  printf( "Got %d events and %d scaler events...\n", events, sevents);

  return 0;
}

static int FileExists(const char *fname)
{
  struct stat buf;

  if( stat( fname, &buf) )
    return 0;
  else
    return 1;
}


static TMapFile *FindMapFile(const char *fname, const int size, int recreate=0)
{
  TMapFile *tmp;

  // Set up a shared memory file for on-line histograms
  //  TMapFile::SetMapAddress(0x42cc2000); 
  //  TMapFile::SetMapAddress(0xb571a000); 
  //  TMapFile::SetMapAddress(0xb5738000); 
  //  TMapFile::SetMapAddress(0xb6768000); 
  //  TMapFile::SetMapAddress(0xb4f20000); 
  //  TMapFile::SetMapAddress(0xb3f7b000);
  TMapFile::SetMapAddress(0xb2023000);

  // Create a new file?
  if( ! recreate && FileExists( fname ) )
    {
      tmp = TMapFile::Create( fname, "UPDATE", size, "On-line histos");
      tmp->RemoveAll();
    }
  else
    tmp = TMapFile::Create( fname, "RECREATE", size, "On-line histos");

  tmp->Update();
  tmp->Print();

  if( ! tmp->IsWritable() )
    {
      fprintf( stderr, "Map file is not writable!!!\nTrying to recreate map file!\n");

      tmp = TMapFile::Create( fname, "RECREATE", size, "On-line histos");

      if( ! tmp->IsWritable() )
	fprintf( stderr, "Map file still not writable !!!!!!!\n");
    }
  return tmp;
}
