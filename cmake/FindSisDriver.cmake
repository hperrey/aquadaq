# - Try to find Sis1100/3100 driver package needed for accessing the VME crate controller via PCI->optical link
# Once done this will define
#  SIS3100_FOUND - System has Sis3100
#  SIS3100_INCLUDE_DIRS - The Sis3100 include directories
#  SISDRIVER_BASE_DIR - The Sis3100 include directories
#  SISDRIVER_DEFINITIONS - Compiler switches required for using Sis3100

macro(find_sis3100_in_driver_dir arg)
# disable a warning about changed behaviour when traversing directories recursively (wrt symlinks)
IF(COMMAND cmake_policy)
  CMAKE_POLICY(SET CMP0009 NEW)
  CMAKE_POLICY(SET CMP0011 NEW) # disabling a warning about policy changing in this scope
ENDIF(COMMAND cmake_policy)
# determine path to sis3100 package in ./driver folder
file(GLOB_RECURSE driver_file ${PROJECT_SOURCE_DIR}/driver/*sis3100_vme_calls.h)
if (driver_file)
  # might have found multiple files of that name) -- should be treating this separately here
#  FOREACH (this_file ${driver_file})
#  	  IF(NOT "${this_file}" MATCHES ".*windows.?7/Inc/")
#	        SET(sis_inc_path "${this_file}")
#	  ENDIF()
#  ENDFOREACH(this_file)
#  IF (sis_inc_path)
    # strip the file name and first directory away:
    get_filename_component(driver_lib_path "${driver_file}" PATH)
    get_filename_component(driver_lib_path "${driver_lib_path}" PATH)
#    MESSAGE(STATUS "Found Sis driver package in 'driver' subfolder: ${driver_lib_path}")
#  ENDIF()
endif(driver_file)

find_path(SIS3100_INCLUDE_DIR sis3100_vme_calls.h
    HINTS "${driver_lib_path}/sis3100_calls" ${arg})
set(SISDRIVER_BASE_DIR ${driver_lib_path} CACHE PATH "Directory of the SIS driver package" FORCE)

endmacro()


IF(NOT SIS3100_INCLUDE_DIR)
   find_sis3100_in_driver_dir("")
ENDIF(NOT SIS3100_INCLUDE_DIR)

set(SIS3100_INCLUDE_DIRS ${SIS3100_INCLUDE_DIR} )
set(SISDRIVER_DEFINITIONS "-DSIS" )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set SIS3100_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(Sis3100  DEFAULT_MSG
                                  	   SIS3100_INCLUDE_DIR SISDRIVER_BASE_DIR SISDRIVER_DEFINITIONS)

mark_as_advanced(SIS3100_INCLUDE_DIR SISDRIVER_BASE_DIR SISDRIVER_DEFINITIONS)
