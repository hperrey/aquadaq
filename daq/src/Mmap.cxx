#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sstream>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "DAQ.h"
#include "Mmap.h"

#include <configVariables.h> // parameters generated during CMake config step

struct run fill;

const int size=1024;
struct run *tmp;
int fd;

struct run *InitMem(int init)
{
  char errstr[128];
  std::stringstream ss;
  ss << DATA_PATH << "/rrunstat.tmp";

  if( (fd = open( ss.str().c_str(), (O_RDWR | O_CREAT), 0600)) == -1 )
    {
      snprintf( errstr, 127, "Could not open mm file %s\n", ss.str().c_str());
      perror( errstr );
    }

  if( init )
    write( fd, &fill, sizeof fill);

  /* Now map the file */
  tmp = (struct run *)mmap( 0, size, (PROT_READ | PROT_WRITE), MAP_SHARED, fd, 0);
  if( tmp == MAP_FAILED )
    {
      snprintf( errstr, 127, "Could not mmap file %s\n", ss.str().c_str());
      perror( errstr );
    }

  return tmp;

}

void UnMem()
{
  munmap( (char *)tmp, size );
  close( fd );
}

void Print( struct run *r)
{
  printf("RunState   : %d\n", r->RunState);
  printf("DoWrite    : %d\n", r->DoWrite);
  printf("DoFill     : %d\n", r->DoFill);
  printf("HistReset  : %d\n", r->HistReset);
  printf("RunNumber  : %ld\n", r->RunNumber);
  printf("nEvents    : %ld\n", r->nEvents);
  printf("fPrefix    : %s\n", r->fPrefix);
  printf("Experiment : %s\n", r->Experiment);

  return;
}
