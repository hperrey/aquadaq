////////////////////////////////////////////////////////////////////////////
// 
// LeCroy 2259  (CAMAC)
// 2259A 12-Channel, 11-Bit Peak-Sensing Analog-to-Digital Converter
// 
// The Model 2259A 12-Channel CAMAC ADC is based largely on the design of the
// widely used Model 2249W. Digital sections are identical, utilizing the
// same synchronized oscillator circuit and low-power LeCroy Model SC-100 Hybrid 
// scaler section, directly interchangeable in either ADC. The analog front
// end of the 2259A is the LeCroy Model VT-100C Voltage-to-Time Converter, 
// offering superior linearity and stability over previous versions.
// 
// The Model 2259A accepts negative-going analog inputs up to -2 volts in 
// amplitude within its linear dynamic range, giving an 11-bit digital output 
// proportional to the highest point of the pulse falling within an externally 
// applied gate interval. The resultant ADC sensitivity is approximately 
// -1 mV/count. The analog input signal should have at least a 100 nsec risetime, 
// or total FWHM duration from 200-400 nsec. The minimum recommended gate duration 
// is 100 nsec, and should enclose the negative peak of the input pulse. 
// Digitizing time of the 2259A is fixed at 106 usec. However, for lower resolution 
// applications, this time may be factory-adjusted downward.
// 
// In common with all new LeCroy CAMAC data acquisition modules, the 2259A offers 
// a fast clear input (< 2 usec total clearing time) to permit fast rejection of 
// unwanted data before, during, or after conversion is complete, eliminating the 
// need for long analog delays. In addi- tion, a front-panel test feature permits
// on-line testing of the entire ADC circuit. When F25 is applied, the 2259A 
// generates an internal 100 nsec gate at S2 time. If the CAMAC I is present, the
// front panel "Test" input will inject a signal with a proportionality constant 
// of -0.167 volt/volt into all inputs. If I is not present, the "Test" input is 
// disabled; F25 will generate the internal gate at S2, but only the residual 
// pedestal at that gate width will be measured.
// 
// The Model 2259A responds to the CAMAC Functions FO,F2,F8,F9,F1O,F24,F25, 
// and F26, and accepts or generates the following CAMAC commands: Z or C, I,Q,X,L. 
// Packaging of the 2259A is a #1 width CAMAC module, conforming to IEEE Report 
// #583. Current usage is low enough to permit the use of up to 23 2259A's 
// (276 channels) in a single, standard, powered CAMAC crate.
//
//
// Dahn Nilsson 2000-02-09
// John Annand  19th Feb. 2014  Modify for SIS1100
///////////////////////////////////////////////////////////////////////////////////
#ifndef _TLRS2259_H_
#define _TLRS2259_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

class TLRS2259: public TObject{

private:

  static const int fNoChannels = 12; // Number of channels

  TCamacBranch *fCBD;           
  Int_t fCrate;			// Crate number
  Int_t fSlot;			// Slot number

  UInt_t pRead[fNoChannels];	// Read addresses
  UInt_t pWrite[fNoChannels]; // Write addresses
  
  UInt_t fClear;	// Clear module
  UInt_t fTest;	// Test module

  void InitUnit();		// Initialize stuff

  //  volatile Short_t *func(int func, int address) // Get address
  //  {return (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, address, func); }

public:

  TLRS2259(TCamacBranch *cbd, int c, int n);
  TLRS2259(TCamacCrate *C, int n);
  virtual ~TLRS2259();

  UShort_t operator[](int n){ return fCBD->Read(pRead[n]);}
  Int_t ReadArray( Short_t *Array, Int_t Count);

  Bool_t Q() {return fCBD->Q();} // Q response
  Bool_t X() {return fCBD->X();} // X response

  Bool_t Write(int val, int n){ fCBD->Write(pWrite[n],val); return Q();}

  void Clear() {fCBD->Read(fClear);}
  void Test(int l=3);

  void Print();
  
  ClassDef( TLRS2259, 0 );  
};

#endif // _TLRS2259_HH_
