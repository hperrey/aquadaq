////////////////////////////////////////////////////////////////////////

#include "TDirectory.h"

#include "XHist.h"

ClassImp(XHist)
//_______________________________________________
XHist::XHist()
{
  ListOfXHistos = new THashList(200);  // Set the capacity of the List
}
//_____________________________________________
XHist::~XHist()
{
  ListOfXHistos->Delete();
}
//_________________________________________________________________________
void XHist::CreateHistograms()
{
  int i;
  char hname[64], htitle[128];

  printf( "Creating  X-histograms...\n");
  /*
  for( i = 0; i < 64 ; i++) 
    {
      sprintf( hname, "ht_%d", i);
      sprintf( htitle, "tagger multi-tdc ht_%d", i);
      ht[i] = new TH1F( hname, htitle, 2000, -500, 7500);
      ListOfXHistos->Add(ht[i]);
    }
  sprintf( hname, "htsum");
  sprintf( htitle, "tagger tdc sum");
  htsum = new TH1F( hname, htitle, 2500, -500, 7500);
  ListOfXHistos->Add(htsum);

  sprintf( hname, "htmult");
  sprintf( htitle, "tagger tdc mult");
  htmult = new TH1F( hname, htitle, 64, -0.5, 63.5);
  ListOfXHistos->Add(htmult);

  hthits = new TH1F("hthits","tagger-TDC-hits",64,0,64);
  ListOfXHistos->Add(hthits);

  ht2D = new TH2F("ht2D","time-vs-index",100,100,1100,64,0,64);
  ListOfXHistos->Add(ht2D);

*/


  for( i = 0; i < 2*ENVqdc ; i++) 
    {
      sprintf( hname, "hvq_%d", i);
      sprintf( htitle, "qdc  hvq_%d", i);
      hvq[i] = new TH1F( hname, htitle, 5000, 0, 4999);
      ListOfXHistos->Add(hvq[i]);
    }

  for( i = 0; i < 64 ; i++) 
    {
      sprintf( hname, "hvt2_%d", i);
      sprintf( htitle, "multi-tdc hvt2_%d", i);
      hvt2[i] = new TH1F( hname, htitle, 2500, -2000, 8000);
      ListOfXHistos->Add(hvt2[i]);
    }
  //
  // Crate 1
  // 2259 ADC
  for( i = 0; i < 12 ; i++) 
    {
      sprintf( hname, "hc1_1_%d", i);
      sprintf( htitle, "QDC c1_1_%d", i);
      hc1_1[i] = new TH1F( hname, htitle, 2048, 0, 2048);
      ListOfXHistos->Add(hc1_1[i]);
    }
  for( i = 0; i < 12 ; i++) 
    {
      sprintf( hname, "hc1_2_%d", i);
      sprintf( htitle, "QDC c1_2_%d", i);
      hc1_2[i] = new TH1F( hname, htitle, 2048, 0, 2048);
      ListOfXHistos->Add(hc1_2[i]);
    }
  for( i = 0; i < 12 ; i++) 
    {
      sprintf( hname, "hc1_3_%d", i);
      sprintf( htitle, "QDC c1_3_%d", i);
      hc1_3[i] = new TH1F( hname, htitle, 2048, 0, 2048);
      ListOfXHistos->Add(hc1_3[i]);
    }
  // 2228 TDC
  for( i = 0; i < 8 ; i++) 
    {
      sprintf( hname, "hc1_4_%d", i);
      sprintf( htitle, "TDC c1_4_%d", i);
      hc1_4[i] = new TH1F( hname, htitle, 2048, 0, 2048);
      ListOfXHistos->Add(hc1_4[i]);
    }
  for( i = 0; i < 8 ; i++) 
    {
      sprintf( hname, "hc1_5_%d", i);
      sprintf( htitle, "TDC c1_5_%d", i);
      hc1_5[i] = new TH1F( hname, htitle, 2048, 0, 2048);
      ListOfXHistos->Add(hc1_5[i]);
    }
  // 4208 TDC
  for( i = 0; i < 8 ; i++) 
    {
      sprintf( hname, "hc1_9_%d", i);
      sprintf( htitle, "LTDC c1_9_%d", i);
      hc1_9[i] = new TH1F( hname, htitle, 10000, 0, 10000);
      ListOfXHistos->Add(hc1_9[i]);
    }
  for(i=0; i<4; i++){
    sprintf( hname,"PSD2D_%d",i);
    sprintf( htitle,"S_QDC-vs-L_QDC_%d",i );
    psd2D[i] = new TH2F(hname,htitle,256,0,1023,256,0,2048);
    ListOfXHistos->Add(psd2D[i]);
  }

  for(i=0; i<4; i++){
    sprintf( hname,"PSD_%d",i);
    sprintf( htitle,"(L_QDC-S_QDC)/L_QDC-vs-L_QDC_%d",i );
    psd[i] = new TH2F(hname,htitle,256,0,1,256,0,2048);
    ListOfXHistos->Add(psd[i]);
  }

  //
  printf( "XHistograms Created!\n");
}

//___________________________________________________________________________
void XHist::FillHistograms( XEvent *xev)
{
  int i,j,k,n;
  UInt_t ref;
  //
  // tdc (tagger)
  //
  /*
  TClonesArray *vt = xev->GetTagger();
  n = xev->GetNTagg();
  //  htmult->Fill(n-1);
  TTagg *t;
  // Find reference
  for( i = 0; i < n; i++){
    t = (TTagg *)vt->operator[](i);
    if ( t->Index() == 63){
      ref =  t->Data();
      break;
    }
  }
  Int_t sub;
  if ( ref != 0){
    for( i = 0; i < n; i++){
      t = (TTagg *)vt->operator[](i);
      sub = (Int_t)(t->Data() - ref);
      //      ht[t->Index()]->Fill(sub);
      //if( t->Index() == 63 ) continue;
      //htsum->Fill( sub + fpOffset[t->Index()] );
      //hthits->Fill(t->Index());
      //ht2D->Fill( sub + fpOffset[t->Index()],t->Index() );
    }
  }
  */
  //
  // Multi hit TDC
  TClonesArray *vtm = xev->GetVtdc();
  n = xev->GetNVtdc();
  // Find ref channel 63
  ref = 0;
  TVtdc* tv;
  for( i = 0; i < n; i++){
    tv = (TVtdc *)vtm->operator[](i);
    if ( tv->Index() == 63){
      ref =  tv->Data();
      //printf("n=%u  ref=%u\n",n,ref);
      break;
    }
  }
  if (ref != 0){
    for( i = 0; i < n; i++) 
      {
	tv = (TVtdc *)vtm->operator[](i);
	Int_t att = (Int_t)(tv->Data() - ref);
	hvt2[tv->Index()]->Fill( att );
	//printf("index=%u data=%u\n",tv->Index(),tv->Data());
      }
  }
  
  //
  //  QDC
  TClonesArray *vq = xev->GetVqdc();
  n = xev->GetNVqdc();
  for( i = 0; i < n; i++) 
    {
      TVqdc *q = (TVqdc *)vq->operator[](i);
      Int_t qi = q->Index();
      //Int_t j;
      hvq[qi]->Fill( q->Data());
      // if( qi < 16 ){
      //	atq[qi] = q->Data();
      //	if( atT[qi] ) atqS[qi] = atq[qi] - atped[qi];
      //      }
    }


  for( i=0; i<12; i++ ){
    hc1_1[i]->Fill( xev->Getc1_1(i) );
    hc1_2[i]->Fill( xev->Getc1_2(i) );
    hc1_3[i]->Fill( xev->Getc1_3(i) );
  }
  for( i=0; i<8; i++ ){
    hc1_4[i]->Fill( xev->Getc1_4(i) );
    hc1_5[i]->Fill( xev->Getc1_5(i) );
    hc1_9[i]->Fill( xev->Getc1_9(i) );
  }

  for( i=0; i<4; i++ ){
    psd2D[i]->Fill(xev->Getc1_2(i),xev->Getc1_3(i));
  }

  double shortPedestal[4]={79.,25.,18.,0.};
  double longPedestal[4]={45., 33., 35.,0.};

  double high[4]={1190.,985.,985.,1200.};
  double low[4]={1180., 975., 975., 800.};


  for( i=0; i<4; i++ ){
    if( xev->Getc1_4(i) > low[i] && xev->Getc1_4(i) <= high[i] )
      psd[i]->Fill((xev->Getc1_3(i)-longPedestal[i]+shortPedestal[i]-xev->Getc1_2(i))/xev->Getc1_3(i),xev->Getc1_3(i));
  }
}
//____________________________________________________________________________
void XHist::ResetAll()
{
  TH1 *h1;
  TIter next(ListOfXHistos);
  while( (h1 = (TH1F *)next()) )
    h1->Reset();
}
//____________________________________________________________________________
void XHist::WriteAll()
{
  TH1 *h1;
  TIter next(ListOfXHistos);
  while( (h1 = (TH1F *)next() ))
    h1->Write();
}

