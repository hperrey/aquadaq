/////////////////////////////////////////////////////
// 
// MPV930 input/output unit
//
// Magnus Lundin 04-05-17 
//
////////////////////////////////////////////////////
#ifndef _TMpv930_H_
#define _TMpv930_H_

#include "TObject.h"

#define MPV930BASE    0x100000               // Base address 
#define MPV930SIZE    0x1000	             // Size of memory region

#if BUSADAPTER == SIS
#define MPV930DEVFILE "/dev/sis110032d16"
#elif BUSADAPTER == SBS
#define MPV930DEVFILE "/dev/vmedrv32d16"
#endif

class TMpv930: public TObject{

private:
  volatile UShort_t *base;
  Int_t fd;
  void* mapped_base;
public:
  TMpv930();
  virtual ~TMpv930();
  Int_t Output();

  ClassDef( TMpv930, 0);
};

#endif /* _TMpv930_H_ */
