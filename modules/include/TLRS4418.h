////////////////////////////////////////////////////////////////////////////
// 
// LeCroy 4418 delay and fan-out (CAMAC)
// 16 channel programmable delay, 3-fold fan-out
// 
///////////////////////////////////////////////////////////////////////////////////
#ifndef _TLRS4418_H_
#define _TLRS4418_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

class TLRS4418: public TObject{

private:

  static const int fNoChannels = 16; // Number of channels

  TCamacBranch *fCBD;           
  Int_t fCrate;			// Crate number
  Int_t fSlot;			// Slot number

  volatile UShort_t *fReset;	        // Reset module
  volatile UShort_t *pWrite[fNoChannels]; // Write addresses
  
  void InitUnit();		// Initialize stuff

  volatile Short_t *func(int func, int address) // Get address
    {return (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, address, func); }

public:

  TLRS4418(TCamacBranch *cbd, int c, int n);
  TLRS4418(TCamacCrate *C, int n);
  virtual ~TLRS4418();

  void SetDelay( UShort_t ch, UShort_t val);
  void SetDelays( UShort_t val);

  Bool_t Q() {return fCBD->Q();} // Q response
  Bool_t X() {return fCBD->X();} // X response

  ClassDef( TLRS4418, 0 )  
};

#endif // _TLRS4418_HH_
