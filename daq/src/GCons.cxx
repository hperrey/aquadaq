// 
// GCons
// Graphical interface to DAQ
// Code borrowed from guitest.cxx

#include <stdlib.h>
#include <unistd.h> // for usleep

#include <TROOT.h>
#include <TApplication.h>
//#include <TGXW.h>
 
#include <configVariables.h> // parameters generated during CMake config step

#include "ConsLib.h"
#include "GCons.h"

enum ETestCommandIdentifiers {
   M_FILE_OPEN,
   M_FILE_SAVE,
   M_FILE_SAVEAS,
   M_FILE_EXIT,
 
   M_TEST_DLG,
   M_TEST_MSGBOX,
   M_TEST_SLIDER,
 
   M_HELP_CONTENTS,
   M_HELP_SEARCH,
   M_HELP_ABOUT,
 
   M_CASCADE_1,
   M_CASCADE_2,
   M_CASCADE_3,
 
   VId1,
   HId1,
   VId2,
   HId2,
 
   VSId1,
   HSId1,
   VSId2,
   HSId2
};
 
#define STARTBUTTON 150
#define STOPBUTTON 200
#define PAUSEBUTTON 225
#define RESETHISTBUTTON 250

 
Int_t mb_button_id[9] = { kMBYes, kMBNo, kMBOk, kMBApply,
                          kMBRetry, kMBIgnore, kMBCancel,
                          kMBClose, kMBDismiss };
 
EMsgBoxIcon mb_icon[4] = { kMBIconStop, kMBIconQuestion,
                           kMBIconExclamation, kMBIconAsterisk };
 
const char *filetypes[] = { "All files",     "*",
                            "ROOT files",    "*.root",
                            "ROOT macros",   "*.C",
                            0,               0 };

// needed to link against ROOT dict:
ClassImp(TestMainFrame);

TestMainFrame::TestMainFrame(const TGWindow *p, UInt_t w, UInt_t h)
      : TGMainFrame(p, w, h)
{
  // init (config) variables to default values
  iRolloverTimeout = 4*60*60*1000; // in [ms]

  // Create test main frame. A TGMainFrame is a top level window.
 
   f1 = new TGCompositeFrame(this, 60, 20, kVerticalFrame);

   AddFrame(f1, new TGLayoutHints(kLHintsBottom | kLHintsExpandX,
            0, 0, 1, 0));

   //
   // Create status frame containing a button and a text entry widget
   //
   fStatusFrame = new TGCompositeFrame(f1, 60, 20, kHorizontalFrame |
                                                     kSunkenFrame);
 
   f1->AddFrame(fStatusFrame, new TGLayoutHints(kLHintsBottom | kLHintsExpandX,
            0, 0, 1, 0));

   fLExperiment = new TGLabel(fStatusFrame, new TGString("Experiment:"));

   fStatusFrame->AddFrame(fLExperiment, new TGLayoutHints(kLHintsTop |
                          kLHintsLeft, 2, 0, 2, 2));
   fTxtExperiment = new TGTextEntry(fStatusFrame, new TGTextBuffer(100));
   fTxtExperiment->Resize(300, fTxtExperiment->GetDefaultHeight());
   fStatusFrame->AddFrame(fTxtExperiment, new TGLayoutHints(kLHintsTop | kLHintsLeft,
                                                       11, 2, 2, 2));

   ReadExperiment();		// Get the experiment line

   //
   // Create options frame
   //
   //-- Run number
   fOptionFrame = new TGCompositeFrame(f1, 60, 20, kHorizontalFrame | kSunkenFrame );
   f1->AddFrame(fOptionFrame, new TGLayoutHints(kLHintsBottom | kLHintsExpandX, 0, 0, 1, 0));

   flblTxtRunno = new TGLabel( fOptionFrame, new TGString("Run number: ") );
   fOptionFrame->AddFrame( flblTxtRunno,  new TGLayoutHints(kLHintsTop | kLHintsLeft,
                                                       2, 0, 2, 2));
   fTxtRunno = new TGTextEntry(fOptionFrame, new TGTextBuffer(100));
   fTxtRunno->Resize(100, fTxtRunno->GetDefaultHeight());

   SetRunText( GetFRunno() );

   fOptionFrame->AddFrame(fTxtRunno, new TGLayoutHints(kLHintsTop | kLHintsLeft,
                                                       2, 2, 2, 2));

   //-- Number of events
   flblTxtEvents = new TGLabel( fOptionFrame, new TGString("Max events: ") );
   fOptionFrame->AddFrame( flblTxtEvents,  new TGLayoutHints(kLHintsTop | kLHintsLeft,
                                                       2, 0, 2, 2));
   fTxtEvents = new TGTextEntry(fOptionFrame, new TGTextBuffer(100));
   fTxtEvents->Resize(100, fTxtEvents->GetDefaultHeight());

   fOptionFrame->AddFrame(fTxtEvents, new TGLayoutHints(kLHintsTop | kLHintsLeft,
                                                       2, 2, 2, 2));

   //--------- create check and radio buttons groups
 
   fG1 = new TGGroupFrame(f1, new TGString("Options"));
 
   fL3 = new TGLayoutHints(kLHintsBottom | kLHintsLeft | kLHintsExpandX | kLHintsExpandY,
                           2, 2, 2, 2);

   fL4 = new TGLayoutHints(kLHintsTop | kLHintsLeft,
                           0, 0, 5, 0);
 
   fcbWriteFile = new TGCheckButton(fG1, new TGHotString("Write to file"), -1);
   fG1->AddFrame( fcbWriteFile, fL4);
   fcbFillHist = new TGCheckButton(fG1, new TGHotString("Fill Histograms"), -1);
   fG1->AddFrame( fcbFillHist, fL4);
   fcbRunRollover = new TGCheckButton(fG1, new TGHotString("Autom. run roll-over"), -1);
   fG1->AddFrame( fcbRunRollover, fL4);

   fcbWriteFile->SetState( kButtonDown );
   fcbFillHist->SetState( kButtonDown );
   fcbRunRollover->SetState( kButtonDown );

   f1->AddFrame( fG1, fL3);


   //-- Create Status frame
 
   fG2 = new TGGroupFrame(f1, new TGString("Status"));
  
   flblRunStart = new TGLabel( fG2, new TGString("") );
   fG2->AddFrame( flblRunStart,  new TGLayoutHints(kLHintsTop | kLHintsLeft,
                                                       2, 0, 2, 2));
   flblRunPaused = new TGLabel( fG2, new TGString("") );
   fG2->AddFrame( flblRunPaused,  new TGLayoutHints(kLHintsTop | kLHintsLeft,
                                                       2, 0, 2, 2));
   flblRunStop = new TGLabel( fG2, new TGString("") );
   fG2->AddFrame( flblRunStop,  new TGLayoutHints(kLHintsTop | kLHintsLeft,
                                                       2, 0, 2, 0));
//     flblEvents = new TGLabel( fG2, new TGString("") );
//     fG2->AddFrame( flblEvents,  new TGLayoutHints(kLHintsTop | kLHintsLeft,
//                                                         2, 0, 2, 2));

   f1->AddFrame( fG2, fL3);

   //
   // Create a Frame for buttons
   //
   f3 = new TGCompositeFrame(f1, 60, 20, kHorizontalFrame);
   f1->AddFrame(f3, new TGLayoutHints(kLHintsBottom | kLHintsExpandX,
            0, 0, 1, 0));

   // Buttons
   fStartButton = new TGTextButton(f3, "&Start", STARTBUTTON);
   fStartButton->Associate(this);
   fStartButton->SetToolTipText("Start a new run");
   f3->AddFrame(fStartButton, new TGLayoutHints(kLHintsTop |
                          kLHintsLeft, 2, 2, 2, 2));
   

   fStopButton = new TGTextButton(f3, "Sto&p", STOPBUTTON);
   fStopButton->Associate(this);
   fStopButton->SetToolTipText("Stop the run");
   fStopButton->SetState( kButtonDisabled );

   f3->AddFrame(fStopButton, new TGLayoutHints(kLHintsTop |
                          kLHintsLeft, 2, 2, 2, 2));


   fPauseButton = new TGTextButton(f3, "&Toggle Pause", PAUSEBUTTON);
   fPauseButton->Associate(this);
   fPauseButton->SetToolTipText("Toggle Pause Run");
   fPauseButton->SetState( kButtonDisabled );

   f3->AddFrame(fPauseButton, new TGLayoutHints(kLHintsTop |
                          kLHintsLeft, 2, 2, 2, 2));


   fResetHist = new TGTextButton(f3, "&Reset all histograms", RESETHISTBUTTON);
   fResetHist->Associate(this);
   fResetHist->SetToolTipText("Reset all histograms");

   f3->AddFrame(fResetHist, new TGLayoutHints(kLHintsTop |
                          kLHintsRight, 2, 2, 2, 2));

   
   SetWindowName(PACKAGE_STRING);
 
   MapSubwindows();
 
   // we need to use GetDefault...() to initialize the layout algorithm...
   Resize(GetDefaultSize());
   //Resize(400, 200);
 
   MapWindow();

   // create timer to monitor run time and schedule timeouts
   Etime = new TTimer();
   Etime->Connect("Timeout()", "TestMainFrame", this, "TimerDone()");
}
 
TestMainFrame::~TestMainFrame()
{
   // Delete all created widgets.
 
   delete fStartButton;
   delete fStopButton;
   delete fPauseButton;
   delete fResetHist;

   delete fLExperiment;
   delete fTxtExperiment;

   delete flblRunStart;
   delete flblRunPaused;
   delete flblRunStop;

   delete flblTxtRunno;
   delete fTxtRunno;

   delete flblTxtEvents;
   delete fTxtEvents;

   delete fcbWriteFile;
   delete fcbFillHist;
   delete fcbRunRollover;

   delete fL3; delete fL4; 

   delete fG1;
   delete fG2;

   delete fOptionFrame;
   delete fStatusFrame;

   delete f3; delete f4; delete f5; delete f2; delete f1;

   // delete other objects
   delete Etime;
}

void TestMainFrame::TimerDone(){
  // timer has run out, this simulates a stop/start press sequence to roll over to the next run
  int Runno = 0;
  char datime[64], str[128];
  TGTextBuffer *tb;
  GetDateTime( datime, 64 );
  tb = fTxtRunno->GetBuffer();
  if( tb->GetTextLength() > 0 ){
      sscanf( tb->GetString(), "%d", &Runno );
  }
  sprintf( str, "Run %d roll-over: %s", Runno, datime);
  flblRunPaused->SetText(new TGString( str ));
  fG2->Layout();
  // "press" stop button
  SetRunStop();
  usleep(5000 * 1000); // sleep for 5s before continuing
  // "press" start button
  SetRunStart();
}

void TestMainFrame::SetRunStart()
{
  // Start run button has been pressed

  int Runno = 0;
  int nevents = 0;

  int DoWrite = ( fcbWriteFile->GetState() == kButtonDown ? 1 : 0);
  int DoFill = ( fcbFillHist->GetState() == kButtonDown ? 1 : 0);
  int DoRunRollover = ( fcbRunRollover->GetState() == kButtonDown ? 1 : 0);
  
  char datime[64], str[128];
  
  GetDateTime( datime, 64 );

  if( fTxtRunno->GetBuffer()->GetTextLength() > 0 )
    sscanf(  fTxtRunno->GetBuffer()->GetString(), "%d", &Runno );
  
  SetRunno( Runno );

  if( fTxtExperiment->GetBuffer()->GetTextLength() > 0 )
      WriteExperiment(  fTxtExperiment->GetBuffer()->GetString() );
  
  if( fTxtEvents->GetBuffer()->GetTextLength() > 0 )
    sscanf(  fTxtEvents->GetBuffer()->GetString(), "%d", &nevents );

#ifndef DRYRUN  
  StartRun( nevents, DoWrite, DoFill );
#endif

  sprintf( str, "Run %d started: %s", Runno, datime);
  
  flblRunStart->SetText( new TGString(str) );
  fG2->Layout();

  fStartButton->SetState( kButtonDisabled );
  fPauseButton->SetState( kButtonUp );
  fStopButton->SetState( kButtonUp );
  fTxtRunno->SetState( 0 );
  if (DoRunRollover) Etime->Start(iRolloverTimeout, kTRUE);   // single-shot timeout in ms
}
  
void TestMainFrame::SetRunStop()
{
  // Stop run button has been pressed
  
  Etime->Stop();   // disable timer for run-rollover
    
  int Runno = 0;
  TGTextBuffer *tb;
  char datime[64], str[128];
  
  GetDateTime( datime, 64 );

#ifndef DRYRUN  
  StopRun();
#endif

  tb = fTxtRunno->GetBuffer();
  if( tb->GetTextLength() > 0 )
    {

      sscanf( tb->GetString(), "%d", &Runno );
      if( Runno > 0 )
        SetRunText( ++Runno );
      else
        tb->Clear();
    }
  
  if( Runno > 0 )               // Save run number if not zero
    SetFRunno( Runno );

  fStartButton->SetState( kButtonUp );
  fPauseButton->SetState( kButtonDisabled );
  fStopButton->SetState( kButtonDisabled );
  fTxtRunno->SetState( 1 );

  if( Runno > 0 )
    sprintf( str, "Run %d stopped: %s", Runno - 1, datime);
  else
    sprintf( str, "Run stopped: %s", datime);

  flblRunStop->SetText( new TGString(str) );
  fG2->Layout();

}
 
void TestMainFrame::SetRunText(int run)
{
  TGTextBuffer *tb;

  tb = fTxtRunno->GetBuffer();
  tb->Clear();
  if( run > 0 )
    {
      char str[64];

      sprintf( str, "%d", run);
      tb->AddText( 0, str );
    }
}
  
int TestMainFrame::GetFRunno()
{
  int runno = 0;

  FILE *fp = fopen(".runno", "r");

  if( fp )
    {
      fscanf( fp, "%d", &runno);
      fclose(fp);
    }
  return runno;
} 

void TestMainFrame::SetFRunno(int runno)
{

  FILE *fp = fopen(".runno", "w");

  if( fp )
    {
      fprintf( fp, "%d", runno);
      fclose(fp);
    }
} 

void TestMainFrame::TogglePauseRunWrapper()
{
  char str[128], datime[64];
  int Runno;

  GetDateTime( datime, 64 );  
  int DoRunRollover = ( fcbRunRollover->GetState() == kButtonDown ? 1 : 0);

  if( fTxtRunno->GetBuffer()->GetTextLength() > 0 )
    sscanf(  fTxtRunno->GetBuffer()->GetString(), "%d", &Runno );
  
  if ( fStopButton->GetState() == kButtonUp) // we are running
    {
      fStopButton->SetState( kButtonDisabled );
      sprintf( str, "Run %d paused: %s", Runno, datime);
      Etime->Stop();   // disable timer for run-rollover
    }
  else // we were paused
    {
      fStopButton->SetState( kButtonUp );
      sprintf( str, "Run %d resumed: %s", Runno, datime);
      if (DoRunRollover) Etime->Start(iRolloverTimeout,kTRUE);   // start single-shot timer
    }

  flblRunPaused->SetText( new TGString(str) );
  flblRunStop->SetText("");
  fG2->Layout();

  TogglePauseRun();
} 

void TestMainFrame::ResetHist()
{
  SetHistReset( 1 );
} 

void TestMainFrame::WriteExperiment(const char *s)
{

  if( *s )
    {
      FILE *fp = fopen(".experiment", "w");

      if( fp )
        {
          fprintf( fp, "%s", s);
          fclose(fp);
        }
      SetExperiment( s );
    }
} 

void TestMainFrame::ReadExperiment()
{

  FILE *fp = fopen(".experiment", "r");

  if( fp )
    {
      TGTextBuffer * tb = fTxtExperiment->GetBuffer();
      char str[128];

      fgets( str, 128, fp);
      if( *str )
        {
          tb->Clear();
          tb->AddText( 0, str );
        }
      fclose(fp);
    }
} 

void TestMainFrame::CloseWindow()
{
   // Got close message for this MainFrame. Calls parent CloseWindow()
   // (which destroys the window) and terminate the application.
   // The close message is generated by the window manager when its close
   // window menu item is selected.
 
   CloseRun();

   TGMainFrame::CloseWindow();
   gApplication->Terminate(0);
}
 
Bool_t TestMainFrame::ProcessMessage(Long_t msg, Long_t parm1, Long_t)
{
   // Handle messages send to the TestMainFrame object. E.g. all menu button
   // messages.
 
   switch (GET_MSG(msg)) {
 
      case kC_COMMAND:
         switch (GET_SUBMSG(msg)) {
 
            case kCM_BUTTON:
	      //	      printf("Button was pressed, id = %ld\n", parm1);
	      if (parm1 == STARTBUTTON)
                SetRunStart();
	      else if (parm1 == STOPBUTTON)
                SetRunStop();
	      else if (parm1 == PAUSEBUTTON)
                TogglePauseRunWrapper();
	      else if (parm1 == RESETHISTBUTTON)
                ResetHist();
	      break;
 
         }
      default:
         break;
   }
   return kTRUE;
}
  
 
//---- Main program ------------------------------------------------------------

extern void InitGui();
VoidFuncPtr_t initfuncs[] = { InitGui, 0 };
 
TROOT root("GUI", "GUI test environement", initfuncs);
 
int main(int argc, char **argv)
{
   TApplication theApp("App", &argc, argv);
 
   TestMainFrame mainWindow(gClient->GetRoot(), 400, 220);
   
   InitRun();

   theApp.Run();
 
   return 0;
}
