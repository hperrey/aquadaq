////////////////////////////////////////////////////////////////////////////
// 
// LeCroy 2228  (CAMAC)
// 2228 Octal Time Digitizer
// 
// The LeCroy Models 2228A and 2229 are Octal Time Digitizers packaged
// in single-width CAMAC modules. Internally, the 2228A and 2229 are 
// identical. They differ only in that the 2228A accepts NIM level inputs
// while the 2229 requires complementary ECL (Emitter Coupled Logic) 
// logic levels.
// 
// The Models 2228A and 2229 have eight independent channels, each of
// which measures the time from the leading edge of a common START pulse
// to the leading edge of its individual STOP pulse. Each channel disregards
// any STOP pulses received before a START signal and will accept only 
// one STOP for every START.
// 
// Conversion begins upon receipt of the START signal and proceeds until
// one of the following: a STOP signal is received; the cycle is terminated
// by the application of a front-panel CLEAR signal; or the digitizer reaches
// full scale.
// 
// The 2228A or 2229 converts the measured time intervals into 11-bit digital
// numbers using a 20 MHz internal clock for a full-scale digitizing time of
// 100 �sec. Rear-panel control of full scale and conversion slope permits 
// digitization to fewer bits and correspondingly shorter conversion time if
// desired. The conversion clock is started in phase with the TDC start signal
// to assure synchronization and eliminate the inaccuracy introduced by the
// free-running oscillators in conventional designs. A CAMAC LAM (Look-At-Me),
// if enabled, is generated at the end of the conversion interval to signal readout.
// 
// Three switch-selectable full-scale time ranges, 100, 200 and 500 nsec, are 
// digitized to 95% of 11 bits (2048 counts) and provide 50, 100 and 250 psec 
// resolutions respectively. Longer time ranges (up to 10 �sec) may be provided 
// on request at slight expense of stability and accuracy.
//
// http://www.lecroy.com/lrs/dsheets/2228.htm
//  
//
// Dahn Nilsson 2000-02-09
// John Annand  19th Feb 2014  Modify for SIS1100
///////////////////////////////////////////////////////////////////////////////////
#ifndef _TLRS4208_H_
#define _TLRS4208_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

class TLRS4208: public TObject{

private:

  static const int fNoChannels = 8; // Number of channels

  TCamacBranch *fCBD;
  Int_t fCrate;			// Crate number
  Int_t fSlot;			// Slot number

  UInt_t pRead[fNoChannels];	// Read addresses
  UInt_t pReadH[fNoChannels];	// Read addresses high bits
  UInt_t pWrite[fNoChannels]; // Write addresses
  
  UInt_t fClear;	// Clear module
  UInt_t fTest;	// Test module

  void InitUnit();		// Initialize stuff

  //volatile Short_t *func(int func, int address) // Get address
  //  {return (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, address, func); }

public:

  TLRS4208(TCamacBranch *cbd, int c, int n);
  TLRS4208(TCamacCrate *C, int n);
  virtual ~TLRS4208();

  UInt_t Read(int n){
    return (fCBD->Read(pReadH[n])<<16) | (fCBD->Read(pRead[n]) & 0xffff);
  }
  Int_t ReadArray( UInt_t *Array, Int_t Count);
  UInt_t operator[](int n){ return Read(n);}

  Bool_t Q() {return fCBD->Q();} // Q response
  Bool_t X() {return fCBD->X();} // X response

  Bool_t Write(int val, int n){ fCBD->Write(pWrite[n],val);  return Q();}

  void Clear() { fCBD->Read(fClear) ;}
  void Test(int l=3);

  void Print();
  
  ClassDef( TLRS4208, 0 )  
};

#endif // _TLRS4208_HH_
