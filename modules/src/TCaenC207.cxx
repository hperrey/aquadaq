
#include "TCaenC207.h"

ClassImp( TCaenC207 )

const char description[] = "Caen C207 Discriminator";  


TCaenC207::TCaenC207(TCamacBranch *cbd, int c, int n): 
fCBD( cbd ), fCrate(c), fSlot(n)
{
  InitUnit();
  EnableChannels( 0xffff);  // Enable all
  SetThresholds( 0x0003);   // Set as low as possible
}

TCaenC207::TCaenC207(TCamacCrate *C, int n):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{
  InitUnit();
  EnableChannels( 0xffff);  // Enable all
  SetThresholds( 0x0003);   // Set as low as possible
}

TCaenC207::~TCaenC207( )
{
}

void TCaenC207::InitUnit()
{
  if( ! fCBD->Online( fCrate ) )
    {
      printf("Camac module %s in crate %d slot %d: file %s line %d\n", 
	     description, fCrate, fSlot, __FILE__, __LINE__);            
      fprintf( stderr, "Warning: Crate is not online\n");
    }

  fReset = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 0, 9);
  fEnableChannels = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 0, 17);
  fTestOutputs = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 0, 25);

  for( int i = 0; i < fNoChannels ; i++)
    {
      pRead[i] = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, i, 0);
      pWrite[i] = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, i, 16);
    }

  return;
}


void TCaenC207::EnableChannels( UShort_t bitp)
{
  *fEnableChannels = (0xffff & bitp);

  return;
}

void TCaenC207::SetThreshold( UShort_t ch, UShort_t val)
{
  *pWrite[ch] = val;

  return;
}

void TCaenC207::SetThresholds( UShort_t val)
{
  for ( UShort_t i = 0; i < fNoChannels; i++)
    SetThreshold( i, val);

  return;
}

void TCaenC207::PrintThresholds()
{
  for ( UShort_t i = 0; i < fNoChannels; i++)
    fprintf( stderr, "ch %d \t threshold %u\n", i, 0x00ff & *pRead[i]);

  return;
}

