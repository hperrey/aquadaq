/////////////////////////////////////////////////////
// 
// Caen 830 32 Channel Multievent Latching Scaler
//
// Magnus Lundin 02-07-19 (loosely based on TStruckDL515)
// Erik Ljungbertz 04-09-10
// Magnus Lundin 050602
// John Annand 19/02/14 Modify for TSIS1100 interface class
//
////////////////////////////////////////////////////
#ifndef _TCaen830_H_
#define _TCaen830_H_

#include "TObject.h"

class TSIS1100;
class TCaen830: public TObject{
private:
  TSIS1100* fSIS;
  UInt_t fbase;                  // Base address for io
  UInt_t hwaddress;                      // The hwaddress
  UShort_t debug;                        // Debug flag
public:
  TCaen830( TSIS1100*, UInt_t, UShort_t= 0);  // hw address 
  virtual ~TCaen830();

  // Generic write/read access to the 16 bit mem area
  void SetShort( UShort_t address, UShort_t value);
  UShort_t GetShort( UShort_t address);
  UShort_t GetShort( UShort_t address, UShort_t bitmask);
  // Generic write/read access to 32 bit mem area
  void SetUInt( UShort_t address, UInt_t value);
  UInt_t GetUInt(UShort_t address);

  void SoftwareClear() { SetShort(0x1122, 0x0001);} // section 3.21
  void SoftwareReset() { SetShort(0x1120, 0x0001);}  // section 3.20
  void EnableChannels( UInt_t bitmask);  // enable channels according to the bitpattern

  UShort_t BufferEmpty();                             // is the data buffer empty
  UShort_t BufferFull();                              // is the data buffer full
  void SoftwareTrigger(){ SetShort( 0x1124, 0x0001);};  // see manual, section 3.22
  UShort_t Busy(){ if (  (GetShort( 0x110e) & 0x0010) == 0x0010) return 1; else return 0;}
  UShort_t InGroundstate();   // Check if the module is in preferred state   
  UShort_t GetData( UShort_t *ch, UInt_t *val);

  void Info();               
  void PrintStatus();
  void bitprint( UInt_t buffer);
  Short_t Test( UInt_t testValue);
  void PrintChannels();

  ClassDef( TCaen830, 0 )
};

#endif /* _TCaen830_H_ */




