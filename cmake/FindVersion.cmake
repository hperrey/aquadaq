# determines the version of our project from git (if available)

# Check if this is a source tarball build
IF(NOT IS_DIRECTORY ${CMAKE_SOURCE_DIR}/.git)
  SET(SOURCE_PACKAGE 1)
ENDIF(NOT IS_DIRECTORY ${CMAKE_SOURCE_DIR}/.git)

# Set package version
IF(NOT SOURCE_PACKAGE)
  # Get the version from last git tag plus numer of additional commits:
  FIND_PACKAGE(Git QUIET)
  IF(GIT_FOUND)
    EXECUTE_PROCESS(COMMAND ${GIT_EXECUTABLE} describe --tags HEAD WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} OUTPUT_VARIABLE ${PROJECT_NAME}_FULL_VERSION ERROR_VARIABLE GIT_ERROR)
    IF(GIT_ERROR) # git execution resulted in error message -- cannot trust result but construct version from hard-set variable
      MESSAGE(WARNING "Could not determine version from git -- git execution resulted in error: ${GIT_ERROR}")
      SET(${PROJECT_NAME}_FULL_VERSION ${${PROJECT_NAME}_VERSION_MAJOR}.${${PROJECT_NAME}_VERSION_MINOR}.${${PROJECT_NAME}_VERSION_PATCH})
    ELSE(GIT_ERROR)
      STRING(REGEX REPLACE "([v0-9.]+[-a-zA-Z0-9]+)-([0-9]+)-([A-Za-z0-9]+)" "\\1+\\2~\\3" ${PROJECT_NAME}_FULL_VERSION ${${PROJECT_NAME}_FULL_VERSION})
      STRING(REGEX REPLACE "\n" "" ${PROJECT_NAME}_FULL_VERSION ${${PROJECT_NAME}_FULL_VERSION}) # remove potential line breaks
      STRING(REGEX REPLACE "v([v0-9.]+)(.*)" "\\1" ${PROJECT_NAME}_BASEVERSION ${${PROJECT_NAME}_FULL_VERSION})
      IF("${${PROJECT_NAME}_BASEVERSION}" VERSION_LESS "${${PROJECT_NAME}_VERSION_MAJOR}.${${PROJECT_NAME}_VERSION_MINOR}.${${PROJECT_NAME}_VERSION_PATCH}")
	MESSAGE(WARNING "Your do not seem to have fetched the latest tags in your git repository -- please consider running 'git fetch upstream'")
      ENDIF("${${PROJECT_NAME}_BASEVERSION}" VERSION_LESS "${${PROJECT_NAME}_VERSION_MAJOR}.${${PROJECT_NAME}_VERSION_MINOR}.${${PROJECT_NAME}_VERSION_PATCH}")
      EXEC_PROGRAM(git ARGS status --porcelain ${CMAKE_CURRENT_SOURCE_DIR} OUTPUT_VARIABLE ${PROJECT_NAME}_MAIN_STATUS)
      IF(${PROJECT_NAME}_MAIN_STATUS STREQUAL "")
	MESSAGE(STATUS "Main ${PROJECT_NAME} directory is clean (all changes have been commited).")
      ELSE(${PROJECT_NAME}_MAIN_STATUS STREQUAL "")
	MESSAGE(STATUS "Main ${PROJECT_NAME} directory is dirty (uncommitted changes present):\n ${${PROJECT_NAME}_MAIN_STATUS}.")
	SET(${PROJECT_NAME}_FULL_VERSION ${${PROJECT_NAME}_FULL_VERSION}*)
      ENDIF(${PROJECT_NAME}_MAIN_STATUS STREQUAL "")
    ENDIF(GIT_ERROR)
  ELSE(GIT_FOUND)
    # If we don't have git we take the hard-set version.
    SET(${PROJECT_NAME}_FULL_VERSION ${${PROJECT_NAME}_VERSION_MAJOR}.${${PROJECT_NAME}_VERSION_MINOR}.${${PROJECT_NAME}_VERSION_PATCH})
  ENDIF(GIT_FOUND)
ELSE(NOT SOURCE_PACKAGE)
  # If we don't have git we take the hard-set version.
  SET(${PROJECT_NAME}_FULL_VERSION "v${${PROJECT_NAME}_VERSION_MAJOR}.${${PROJECT_NAME}_VERSION_MINOR}.${${PROJECT_NAME}_VERSION_PATCH}")
ENDIF(NOT SOURCE_PACKAGE)
MESSAGE("-- Determined ${PROJECT_NAME} version ${${PROJECT_NAME}_FULL_VERSION}")
