////////////////////////////////////////////////////////////////////////////
// 
// LeCroy 4416 high-speed discrimintaor (CAMAC)
// 16 channels
// John Annand 20th Feb. 2014  Modify for SIS1100
// 
///////////////////////////////////////////////////////////////////////////////////
#ifndef _TLRS4416_H_
#define _TLRS4416_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

class TLRS4416: public TObject{

private:
  static const int fNoChannels = 16; // Number of channels
  TCamacBranch *fCBD;           
  Int_t fCrate;			// Crate number
  Int_t fSlot;			// Slot number

  UInt_t pTest;               // Test
  UInt_t pMask;               // Mask
  UInt_t pReadThresh;         // Read threshold
  UInt_t pWriteThresh;        // Write threshold
  UInt_t pTestRemote;         // test local/remote
  void InitUnit();     	      // Initialize stuff
public:
  TLRS4416(TCamacBranch *cbd, int c, int n);
  TLRS4416(TCamacCrate *C, int n);
  virtual ~TLRS4416();

  void MaskChannels( UShort_t bitpattern);
  void PrintThreshold();
  void SetThreshold( UShort_t val);
  void Test();

  Bool_t Q() {return fCBD->Q();} // Q response
  Bool_t X() {return fCBD->X();} // X response

  ClassDef( TLRS4416, 0 )  
};

#endif // _TLRS4416_HH_
