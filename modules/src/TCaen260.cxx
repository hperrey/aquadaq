// 
// Class implementation of the CAEN V260 16 channel scaler
// 
// Dahn Nilsson       2000-03-02
// Magnus Lundin      061130
//
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include "TCaen260.h"

ClassImp( TCaen260 )

const char description[] = "Caen 260 Scaler";
const UShort_t CAEN260SIZE = 0xffff;

#ifdef SIS
char memdev2416[] = "/dev/sis110032d16";
char memdev2432[] = "/dev/sis110032d32";
#endif
#ifdef SBS
char memdev2416[] = "/dev/vmedrv32d16";
char memdev2432[] = "/dev/vmedrv32d32";
#endif

/////////////////////////////////////////////////////////////////////////
TCaen260::TCaen260( UInt_t BASE, UShort_t debugflag)
{
  hwaddress = BASE;
  debug = debugflag;      
  Init( BASE);

  Clear();
  ResetInhibit();
}
/////////////////////////////////////////////////////////////////////////
TCaen260::~TCaen260( )
{
  munmap( (char *)ftmpbase2432, CAEN260SIZE);
  munmap( (char *)ftmpbase2416, CAEN260SIZE);
  close( ffd2432);
  close( ffd2416);
}
/////////////////////////////////////////////////////////////////////////
void TCaen260::SetShort( UShort_t address, UShort_t value)
{
  *(fbase2416 + address/2) = value;
  return;
}
/////////////////////////////////////////////////////////////////////////
UShort_t TCaen260::GetShort( UShort_t address)
{
 return ( *(fbase2416 + address/2) );  
}
/////////////////////////////////////////////////////////////////////////
void TCaen260::Init(  UInt_t BASE)
{          
  if (  (ffd2432 = open( memdev2432, O_RDWR | O_SYNC)) == -1) 
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      //      printf("open() failed: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }
  if ((ftmpbase2432 = (volatile void *)mmap(NULL, CAEN260SIZE, PROT_WRITE, MAP_SHARED, ffd2432, BASE)) == MAP_FAILED)
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      //      printf("mmap() falied: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }
  if ( ftmpbase2432 == (void *)-1 )
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      exit(-1);
    }
  fbase2432 = (volatile UInt_t *)((char *)ftmpbase2432);  


  if (  (ffd2416 = open( memdev2416, O_RDWR | O_SYNC)) == -1) 
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      //      printf("open() failed: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }
  if ((ftmpbase2416 = (volatile void *)mmap(NULL, CAEN260SIZE, PROT_WRITE, MAP_SHARED, ffd2416, BASE)) == MAP_FAILED)
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      //      printf("mmap() falied: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }
  if ( ftmpbase2416 == (void *)-1 )
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      exit(-1);
    }
  fbase2416 = (volatile UShort_t *)((char *)ftmpbase2416);  


  if ( debug > 1)
    {
      fprintf( stderr, "fbase2432 at %p, fbase2416 at %p \n", fbase2432, fbase2416);
    }
}
/////////////////////////////////////////////////////////////////////////
UShort_t TCaen260::GetData( UShort_t *ch, UInt_t *val)
{
  UShort_t i;

  for ( i = 0; i < 16; i++)
    {
      *ch++  = i;
      *val++ = 0x00ffffff & *( fbase2432 + 4 + i);  // offset 0x10 = 4 * 4
    }

  return 16;
} 
/////////////////////////////////////////////////////////////////////////
UInt_t TCaen260::GetChannelData( UShort_t ch)
{
  // offset 0x10 = 4 * 4
  return 0x00ffffff & *( fbase2432 + 4 + ch);  
} 
/////////////////////////////////////////////////////////////////////////
void TCaen260::PrintAll()
{
  UShort_t ch[16], i;
  UInt_t val[16];

  GetData( ch, val);

  for ( i = 0; i < 16; i++)
    printf("ch: %d   val: %d  0x%x\n", ch[i], val[i], val[i]);

  return;
}
