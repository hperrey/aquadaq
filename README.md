# prerequisites

- ROOT (tested 5.34.34)
- SIS 1100/3100 drivers (tested 2.13-9)

## install ROOT
set the environment variables:
. /usr/local/root534/bin/thisroot.sh

## install SIS driver
* find newest driver on homepage: http://www.struck.de/linux1100.htm

### compile and install kernel module
cd daq/driver
wget http://www.struck.de/sis1100-2.13-9.tar.gz
tar xvzf sis1100-2.13-9.tar.gz
cd sis1100-2.13-9/dev/pci
make
su -c "make install"

## install CMake
sudo yum install cmake

# compile from sources
mkdir build
cd build
cmake ..
make
make install

This will build the daq executable, the shared libraries, the module library and the SIS3100 shared library inside the 'build' directory.
The executable will be installed into the 'bin' subdirectory. All libaries can be found in the 'lib' subdirectory. Installation paths can be changed via the CMake INSTALL_PREFIX variable:
cmake -DINSTALL_PREFIX=/some/dir ..
