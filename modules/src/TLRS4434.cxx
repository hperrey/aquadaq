//////////////////////////////////////////////////////////////////////
//
// Implementation of the LeCroy 32-channel scaler 
//
// 000508 Magnus Lundin
// John Annand 19/02/14 Modify for TSIS1100 interface class
// John Annand 18/02/16 ReadClearArray() ensure scalers are cleared
//
//////////////////////////////////////////////////////////////////////
#include "TLRS4434.h"
#include <stdlib.h>

ClassImp( TLRS4434);

TLRS4434::TLRS4434(TCamacCrate *C, int n, int channels):
  fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n), fChannels(channels)
{
 
  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  // Pointer to command register
  pComm = fCBD->cnaf( fCrate, fSlot, 0, 16, 1); // T = 1 

  // Pointer to sequential reading, do a 16 bit access
  pReadClear = fCBD->cnaf( fCrate, fSlot, 0, 2, 1);  // T = 1 

  // Pointer to sequential reading, do a 16 bit access
  pRead      = fCBD->cnaf( fCrate, fSlot, 0, 0, 1);  // T = 1 

  fCBD->Write(pComm,TLRS4434CLEAR);

}

TLRS4434::~TLRS4434( )
{
}

UInt_t TLRS4434::ReadChannelOne()
{
  UInt_t value;

  if ( fChannels < 1)
    {
      printf("TLRS4434 no channels defined\n");
      exit(-1);
    }
  fCBD->Write(pComm,TLRS4434LOAD);
  value = fCBD->Read(pRead-2) << 16;
  value |= fCBD->Read(pRead);
  return value;
}

UShort_t TLRS4434::ReadClearArray( UInt_t *arr)
{
  fCBD->Write(pComm,TLRS4434LOAD);
  for( int i = 0; i < fChannels; i++)
    {
      *arr = fCBD->Read(pReadClear-2) << 16;
      *arr++ |= fCBD->Read(pReadClear);
    }
  fCBD->Write(pComm,TLRS4434CLEAR);
  return fChannels;
}                        

UShort_t TLRS4434::ReadArray( UInt_t *arr)
{
  fCBD->Write(pComm,TLRS4434LOAD);

  for( int i = 0; i < fChannels; i++)
    {
      //*pComm = (0x218 << 4) + i; 
      fCBD->Write(pComm,(0x218<<4)+i);

      *arr = fCBD->Read(pRead-2) << 16;
      *arr++ |= fCBD->Read(pRead);
      //*arr    = *( volatile UShort_t *)(pRead  - 1) << 16; 
      //*arr++ |= *( volatile UShort_t *)pRead;
    }
  return fChannels;
}                        

void TLRS4434::PrintAll()
{

  UInt_t testArray[fChannels];

  ReadClearArray( testArray );   

  for ( int i = 0; i < fChannels; i++)
      printf("Channel %d \t 0x%08x \n", i, testArray[i] );
  
}


void TLRS4434::Test()
{

  UInt_t testArray[fChannels];

  printf("Running selftest. Make sure crate is inhibited!\n");

  for ( int i = 0; i < 255; i++)
    fCBD->Write(pComm,0xbf00);
  // *pComm  = 0xbf00;  // Increment each byte by one

  ReadClearArray( testArray );   

  PrintAll();

  fCBD->Write(pComm,TLRS4434CLEAR);
}


