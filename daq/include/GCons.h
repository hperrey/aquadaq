#ifndef ROOT_GCons
#define ROOT_GCons

#include <TGListBox.h>
#include <TGClient.h>
#include <TGFrame.h>
#include <TGIcon.h>
#include <TGLabel.h>
#include <TGButton.h>
#include <TGTextEntry.h>
#include <TGMsgBox.h>
#include <TGMenu.h>
#include <TGCanvas.h>
#include <TGComboBox.h>
#include <TGTab.h>
#include <TGSlider.h>
#include <TGDoubleSlider.h>
#include <TGFileDialog.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TRandom.h>
#include <TSystem.h>
#include <TEnv.h>


class TestMainFrame : public TGMainFrame {
 
private:
  TGCompositeFrame     *f1, *f2, *f3, *f4, *f5;
  TGCompositeFrame   *fStatusFrame, *fOptionFrame;
  TGCanvas           *fCanvasWindow;
  TGTextEntry        *fTxtExperiment, *fTxtRunno, *fTxtEvents;
  TGButton           *fStartButton;
  TGButton           *fStopButton;
  TGButton           *fPauseButton;
  TGButton           *fResetHist;
  TGCheckButton      *fcbWriteFile, *fcbFillHist, *fcbRunRollover;

  TGGroupFrame       *fG1, *fG2;
  TGLayoutHints      *fL3, *fL4;
  TGLabel            *fLExperiment, *flblTxtRunno, *flblTxtEvents;
  TGLabel            *flblRunStart, *flblRunPaused, *flblRunStop, *flblEvents;

  TTimer             *Etime;
  int                iRolloverTimeout;
  
  int  GetFRunno();
  void SetFRunno(int run);
  void SetRunText( int run );

  void WriteExperiment( const char *s);
  void ReadExperiment();
  void TogglePauseRunWrapper();
  void ResetHist();

public:
   TestMainFrame(const TGWindow *p, UInt_t w, UInt_t h);
   virtual ~TestMainFrame();

   virtual void SetRunStart();
   virtual void SetRunStop();

   virtual void CloseWindow();
   virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t);

   void TimerDone();

   ClassDef(TestMainFrame,0)  //The class title, no object IO needed
};

#endif
