
#include "TLRS4418.h"

ClassImp( TLRS4418 )

const char fDescription[] = "LeCroy delay/fan-out";  


TLRS4418::TLRS4418(TCamacBranch *cbd, int c, int n): 
fCBD( cbd ), fCrate(c), fSlot(n)
{
  InitUnit();
}

TLRS4418::TLRS4418(TCamacCrate *C, int n):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{
  InitUnit();
}

TLRS4418::~TLRS4418( )
{
}

void TLRS4418::InitUnit()
{
  if( ! fCBD->Online( fCrate ) )
    {
      printf("Camac module %s in crate %d slot %d: file %s line %d\n", 
	     fDescription, fCrate, fSlot, __FILE__, __LINE__);            
      fprintf( stderr, "Warning: Crate is not online\n");
    }

  for( int i = 0; i < fNoChannels ; i++)
    pWrite[i] = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, i, 16);

  return;
}


void TLRS4418::SetDelay( UShort_t ch, UShort_t val)
{
  *pWrite[ch] = val;
  return;
}

void TLRS4418::SetDelays( UShort_t val)
{
  // range 0 - 15 (ie 0 -15 ns)
  for ( UShort_t i = 0; i < fNoChannels; i++)
    SetDelay( i, val);

  return;
}



