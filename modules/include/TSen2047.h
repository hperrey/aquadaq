/////////////////////////////////////////////////////
// 
// Sen 2047 bitpattern unit (based on TCamacUnit)
//
// Dahn Nilsson 1999-03-02
// Magnus Lundin 2005-09-24
//
////////////////////////////////////////////////////
#ifndef _TSen2047_H_
#define _TSen2047_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

class TSen2047: public TObject{

private:

  TCamacBranch *fCBD;
  const int fCrate;             // Crate number
  const int fSlot;              // Slot number
  int fChannels;              // Number of channels

  volatile Short_t *pRead;
  volatile Short_t *pReadClear;
  volatile Short_t *pWrite;
  
  volatile Short_t *fClear; // Clear module
  volatile Short_t *fTest; // Test module

public:
  TSen2047(TCamacBranch *cbd, int c, int n);
  TSen2047(TCamacCrate *C, int n);
  virtual ~TSen2047();

  UShort_t Read(){ return *(UShort_t *)pRead;}
  UShort_t ReadClear(){ return *(UShort_t *)pReadClear;}
  void Clear() {if (*fClear) ;}

  Bool_t Q() {return fCBD->Q();} // Q response
  Bool_t X() {return fCBD->X();} // X response

  void Test();
  void Print();
  void BitPrint();
  
  ClassDef( TSen2047, 0)  // Camac unit
};

#endif /* _TSen2047_H_ */
