
#include "TFADC.h"

ClassImp( TFADC );

TFADC::TFADC(TCamacBranch *cbd, int c, int n): 
fCBD( cbd ), fCrate(c), fSlot(n)
{

  if( ! cbd->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fNoChannels = 300;

  fClear = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, 0, 9);
  fTest = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, 0, 25);
  fEnable = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, 0, 24);
  fDisable = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, 0, 26);
  fRead = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, 0, 0);
}

TFADC::TFADC(TCamacCrate *C, int n):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{

  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fNoChannels = 300;

  fClear = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 9);
  fTest = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 25);
  fEnable = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 24);
  fDisable = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 26);
  fRead = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 0);
}

TFADC::~TFADC( )
{
}

Int_t TFADC::ReadArray( Short_t *Array, Int_t count)
{
  // Read an array of count values into Array
  // from channel starting on 0.
  
  int i, max;

  max = count > fNoChannels ? fNoChannels : count;

  for( i = 0; i < max; i++)
    *Array++ = *fRead;

  return i;
}

Bool_t TFADC::Test(int l)
{

  if( *fTest )
    ;

  return Q();

}

void TFADC::Print( )
{
  Short_t d;

  d = *fRead;
  for( int i = 1; i < fNoChannels && Q(); i++)
    {
      printf( "Channel: %d, value: 0x%x (%d)\n", i, d, d);
      d = *fRead;
    }
}
