//Header files that must be included to run properly

#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <fstream>
using namespace std;
#include "TApplication.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TFile.h"
#include "TTree.h"
#include "TF1.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TROOT.h"
#include "TCutG.h"
#include "TClonesArray.h"
#include "TMath.h"
#include "XEvent.h"
#include "ScalerEvent.h"
#include "XHist.h"
#include "ScalerHist.h"
#include "TRandom3.h"

double c = 299792458;

#define MAX_RUNS 300 
char DATAFILE[MAX_RUNS][256] = 

  { 

    "/home/daquser/aquadaq/Data/Data030.root", // PuBe no shielding
    // "/home/kfoto/daq/Data/Data077.root", // 3 GB 
    // "/home/kfoto/daq/Data/Data093.root", //  2.5 GB
    //"/home/kfoto/daq/Data/Data108.root", // 2.4 GB
    //"/home/kfoto/daq/Data/Data126.root", // 12 GB
    //"/home/kfoto/daq/Data/Data135.root", // 3 GB
    //"/home/kfoto/daq/Data/Data198.root", // 2.9 GB no TOF
    //"/home/kfoto/daq/Data/Data241.root", // 2.3 GB concrete block
  };  


 

int main(int argc, char *argv[])

{//open main ++

  
  //    gSystem->Load("libTree");
  TROOT simple("simple", "offline");

 // Create a new rootfile for the results


	
  TFile *output = new TFile("output.root", "RECREATE");

  XEvent *xevent = new XEvent();
  ScalerEvent *scalerevent = new ScalerEvent();

  TTree *TX;
  TBranch *xbranch;

  TTree *TS;
  TBranch *sbranch;

  ofstream nentries_file;
  nentries_file.open ("numbers.txt");

  double pedestal_short[4] = {88.,31.,0,0}; // detector 0, 1, 2, 3
  double pedestal_long[4] = {34.,34.,0,0}; // detector 0, 1, 2, 3

  double bit_high[4] = {1045.,985.,0,0};//{1190.,985.,0,0}; // detector 0, 1, 2, 3
  double bit_low[4] = {1030.,970.,0,0};//{1180.,970.,0,0}; // detector 0, 1, 2, 3

  double offset[4] = {0.,-0.22,9.40,36.18}; // detector 0, 1, 2, 3



  char hname[256], htitle[256];


// Xarm histograms
  
  TH1F *he_short[4];

  TH1F *he_short_neutron[4];

  TH1F *he_long[4];
  
  TH1F *he_long_neutron[4];

  TH2F *hPSD[4];

  TH1F *he_tdc[4];

  TH1F *hTOF_tdc[4];

  TH1F *hTOF_tdc_neutron[4]; 

  TH1F *hTOF[4]; 


  for ( int i = 0; i < 4; i++)
    {

      sprintf( hname, "he_short%d",i);
      sprintf( htitle, "E short gate %d",i);
      he_short[i] = new TH1F ( hname, htitle, 2048, 0, 2047);
      he_short[i] -> GetXaxis()->CenterTitle();
      he_short[i] -> GetXaxis()->SetTitle("QDC [arb. units]");
      he_short[i] -> GetYaxis()->CenterTitle();
      he_short[i] -> GetYaxis()->SetTitle("counts");

      sprintf( hname, "he_long%d",i);
      sprintf( htitle, "E long gate test %d",i);
      he_long[i] = new TH1F ( hname, htitle, 2048, 0, 2047);
      he_long[i] -> GetXaxis()->CenterTitle();
      he_long[i] -> GetXaxis()->SetTitle("QDC [arb. units]");
      he_long[i] -> GetYaxis()->CenterTitle();
      he_long[i] -> GetYaxis()->SetTitle("counts");

      sprintf( hname, "he_short_neutron%d",i);
      sprintf( htitle, "E short gate %d",i);
      he_short_neutron[i] = new TH1F ( hname, htitle, 2048, 0, 2047);
      he_short_neutron[i] -> GetXaxis()->CenterTitle();
      he_short_neutron[i] -> GetXaxis()->SetTitle("QDC [arb. units]");
      he_short_neutron[i] -> GetYaxis()->CenterTitle();
      he_short_neutron[i] -> GetYaxis()->SetTitle("counts");

      sprintf( hname, "he_long_neutron%d",i);
      sprintf( htitle, "E long gate test %d",i);
      he_long_neutron[i] = new TH1F ( hname, htitle, 2048, 0, 2047);
      he_long_neutron[i] -> GetXaxis()->CenterTitle();
      he_long_neutron[i] -> GetXaxis()->SetTitle("QDC [arb. units]");
      he_long_neutron[i] -> GetYaxis()->CenterTitle();
      he_long_neutron[i] -> GetYaxis()->SetTitle("counts");

      sprintf( hname, "hPSD%d",i);
      sprintf( htitle, "PSD %d",i);
      hPSD[i] = new TH2F ( hname, htitle, 2048, 0, 2047, 2048, 0, 1);
      hPSD[i] -> GetXaxis()->CenterTitle();
      hPSD[i] -> GetXaxis()->SetTitle("QDC [arb. units]");
      hPSD[i] -> GetYaxis()->CenterTitle();
      hPSD[i] -> GetYaxis()->SetTitle("PS [arb. units]");

      sprintf( hname, "he_tdc%d",i);
      sprintf( htitle, "Bit TDC %d",i);
      he_tdc[i] = new TH1F ( hname, htitle, 2048, 0, 2047 );
      he_tdc[i] -> GetXaxis()->CenterTitle();
      he_tdc[i] -> GetXaxis()->SetTitle("TDC [arb. units]");
      he_tdc[i] -> GetYaxis()->CenterTitle();
      he_tdc[i] -> GetYaxis()->SetTitle("counts");                                
                                                
      sprintf( hname, "hTOF_tdc%d",i);
      sprintf( htitle, "TOF TDC %d",i);
      hTOF_tdc[i]= new TH1F ( hname, htitle, 2048, 0, 2047 );
      hTOF_tdc[i]->GetXaxis()->CenterTitle();
      hTOF_tdc[i]->GetXaxis()->SetTitle("TDC [arb. units]");
      hTOF_tdc[i]->GetYaxis()->CenterTitle();
      hTOF_tdc[i]->GetYaxis()->SetTitle("counts");

      sprintf( hname, "hTOF_tdc_neutron%d",i);
      sprintf( htitle, "TOF TDC Neutron %d",i);
      hTOF_tdc_neutron[i]= new TH1F ( hname, htitle, 2048, 0, 2047 );
      hTOF_tdc_neutron[i]->GetXaxis()->CenterTitle();
      hTOF_tdc_neutron[i]->GetXaxis()->SetTitle("TDC [arb. units]");
      hTOF_tdc_neutron[i]->GetYaxis()->CenterTitle();
      hTOF_tdc_neutron[i]->GetYaxis()->SetTitle("counts");
                                                
      sprintf( hname, "hTOF%d",i);
      sprintf( htitle, "TOF %d",i);
      hTOF[i]= new TH1F ( hname, htitle, 2048, -50, 500 );
      hTOF[i]->GetXaxis()->CenterTitle();
      hTOF[i]->GetXaxis()->SetTitle("ToF [ns]");
      hTOF[i]->GetYaxis()->CenterTitle();
      hTOF[i]->GetYaxis()->SetTitle("counts");

    }

  /*----------------------------------------------------------------------------------*/
  /*Scalers*/

  TH1F *scaler_entries = new TH1F("scaler_entries","scaler_entries",1,0,1);
  TH1F *free_scalers = new TH1F("free_scalers","Free Scaler counts",64,0,64);

  /*----------------------------------------------------------------------------------*/ 
  int run = 0;
  int event = 0;
  int nentries = 0;
  int sentries = 0;
  char *temp;
   
  //-------------------Loop over runs----------------------
  //
  //


 while(run < MAX_RUNS && DATAFILE[run][0]) 
   {//Loop over runs ++
     
    
     temp = DATAFILE[run];
        
     
     TFile *infile = new TFile(temp,"OPEN");    
     

     printf("------------------------------------------------------------\n");
     
     printf("------------------------------------------------------------\n");  
     fprintf( stderr, "Processing %s\n", DATAFILE[run]);

      
        
     // 
     //-------------------Loop over events----------------------
     //
     
    
     //Open file
     //
     // Scaler
     //
     
     TS =  (TTree*) infile->Get("TS");
     sbranch  = TS->GetBranch("scalerbranch");
     sbranch->SetAddress(&scalerevent);
     sentries = TS->GetEntries();
     cout<<"Number of scaler entries "<<sentries<<endl;
     nentries_file<<"Scalers "<<sentries<<endl;
     
     for ( event = 0; event < sentries; event++) 
       {           
         TS->GetEntry( event);       
         //scalerevent->PrintScalers();
       }
     scaler_entries->SetBinContent(1,sentries);


     for (int j=0;j<32;j++)
       {
         free_scalers->SetBinContent(j+1,scalerevent->Getvs0(j));
         free_scalers->SetBinContent(j+1+32,scalerevent->Getvs1(j));
       }

     //
     // X
     //
     TX = (TTree*)infile->Get("TX");
     xbranch  = TX->GetBranch("xbranch");
     xbranch->SetAddress(&xevent);
  
     nentries = TX->GetEntries();
     
     printf("------------------------------------------------------------\n");

     cout<<"Number of X entries "<<nentries<<endl;
     nentries_file<<"Xevents "<<nentries<<endl;

     printf("------------------------------------------------------------\n");
     

     for ( event = 0; event < nentries; event++) 
       {//Loop over X events ++  
	 
	 TX->GetEntry( event);

         if ( event%1000000 == 0) 
           {
             printf("\n \n x entry No. %d\n", event);
           }
	 
	 for(int i=0; i<4; i++)
	   {	     
	     he_tdc[i]->Fill(xevent->Getc1_4(i));
	     
	     if(xevent->Getc1_4(i) > bit_low[i] && xevent->Getc1_4(i) < bit_high[i] )
	       {	 
		 he_short[i]->Fill(xevent->Getc1_2(i));             
		 he_long[i]->Fill(xevent->Getc1_3(i));
		 hPSD[i]->Fill(xevent->Getc1_3(i), (xevent->Getc1_3(i)-pedestal_long[i]-xevent->Getc1_2(i)+pedestal_short[i])/xevent->Getc1_3(i));
		 for(int j = 0; j<4;j++)
		   {
		     hTOF_tdc[i]->Fill(-(-2000+offset[j] +xevent->Getc1_5(j))); 
		     // if(((xevent->Getc1_3(i)-pedestal_long[i]-xevent->Getc1_2(i)+pedestal_short[i])/xevent->Getc1_3(i) >0.4 && (xevent->Getc1_3(i)-pedestal_long[i]-xevent->Getc1_2(i)+pedestal_short[i])/xevent->Getc1_3(i) <0.6 && xevent->Getc1_3(i) >200.)||((xevent->Getc1_3(i)-pedestal_long[i]-xevent->Getc1_2(i)+pedestal_short[i])/xevent->Getc1_3(i) >0.45 && (xevent->Getc1_3(i)-pedestal_long[i]-xevent->Getc1_2(i)+pedestal_short[i])/xevent->Getc1_3(i) <0.6 && xevent->Getc1_3(i) <200.) )
 if(((xevent->Getc1_3(i)-pedestal_long[i]-xevent->Getc1_2(i)+pedestal_short[i])/xevent->Getc1_3(i) >0.3 && (xevent->Getc1_3(i)-pedestal_long[i]-xevent->Getc1_2(i)+pedestal_short[i])/xevent->Getc1_3(i) <0.6 && xevent->Getc1_3(i) >350.)||((xevent->Getc1_3(i)-pedestal_long[i]-xevent->Getc1_2(i)+pedestal_short[i])/xevent->Getc1_3(i) >0.6 - 0.000857*xevent->Getc1_3(i) && (xevent->Getc1_3(i)-pedestal_long[i]-xevent->Getc1_2(i)+pedestal_short[i])/xevent->Getc1_3(i) <0.6 && xevent->Getc1_3(i) <350.) )
		       {
			 hTOF_tdc_neutron[i]->Fill(-(-2000.+offset[j] +xevent->Getc1_5(j)));

			 if(-(-2000.+offset[j]+xevent->Getc1_5(j)) <600. && -(-2000.+offset[j] +xevent->Getc1_5(j)) >  330.)
			   {
			     he_short_neutron[i]->Fill(xevent->Getc1_2(i));             
			     he_long_neutron[i]->Fill(xevent->Getc1_3(i));
			   }
		       }
		   }
	       }
	   }
	   
       }// End loop over X events --
	 
	 
	 for(int i=0; i<4; i++)
	   {	     
	     he_tdc[i]->Scale(1./sentries);
	     he_short[i]->Scale(1./sentries);             
	     he_long[i]->Scale(1./sentries);
	     he_short_neutron[i]->Scale(1./sentries);             
	     he_long_neutron[i]->Scale(1./sentries);
	     hPSD[i]->Scale(1./sentries);
	     hTOF_tdc[i]->Scale(1./sentries);
	     hTOF_tdc_neutron[i]->Scale(1./sentries);
	   }

     infile->Close();  
     
     run++;
   } // End loop over runs --

 
 nentries_file.close();
 
 output->Write();
 
 delete output;
 delete xevent;
 delete scalerevent;
 
 return 0;
}//close main --
