
#include "TSen2078.h"

ClassImp( TSen2078 )

TSen2078::TSen2078(TCamacBranch *cbd, int c, int n): 
fCBD( cbd ), fCrate(c), fSlot(n)
{

  if( ! cbd->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fClear = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, 0, 9);
  fTest = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, 0, 25);

  pReadLW = (volatile UShort_t *)cbd->cnaf( fCrate, fSlot, 0, 0);
  pReadHW = (volatile UShort_t *)cbd->cnaf( fCrate, fSlot, 1, 0);

  enableCounting = (volatile UShort_t *)cbd->cnaf( fCrate, fSlot, 1, 26);
  disablePredivider = (volatile UShort_t *)cbd->cnaf( fCrate, fSlot, 2, 24);
  presetLW = (volatile UShort_t *)cbd->cnaf( fCrate, fSlot, 0, 16);
  presetHW = (volatile UShort_t *)cbd->cnaf( fCrate, fSlot, 1, 16);

  *enableCounting = 1;
  *disablePredivider = 1;
}

TSen2078::TSen2078(TCamacCrate *C, int n):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{

  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fClear = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 9);
  fTest = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 25);

  pReadLW = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 0, 0);
  pReadHW = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 1, 0);

  enableCounting = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 1, 26);
  disablePredivider = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 2, 24);
  presetLW = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 0, 16);
  presetHW = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 1, 16);

  *enableCounting = 1;
  *disablePredivider = 1;
}

TSen2078::~TSen2078( )
{
}

void TSen2078::Load()
{
  *presetLW = 0xffff;
  *presetHW = 0xffff;
}

UInt_t TSen2078::Read()
{
  UInt_t value;

  value = (*(volatile UShort_t *)pReadHW) << 16;
  value |= *(volatile UShort_t *)pReadLW;

  return value;
}

void TSen2078::Test()
{
  volatile Short_t dum;
  printf("Test sets all 16 bit to 1\n");
  dum = *fTest;
  return;
}

void TSen2078::Print( )
{
  printf( "Value: %x\n", Read());
  return;
}

