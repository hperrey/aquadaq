/////////////////////////////////////////////////////
// 
// Caen 792 32 Channel QDC ( Charge to Digital Converter)
//
// Magnus Lundin 2005-07-27 
//
// 2010-06-18 Changed data readout mask to 0x3fff
//            Changed handling of under and overflows
//            Changed ch No readout mask to 0x1f
//            ctor now leaves module without under and overflow 
//            suppression. The user must explicitly request this 
//            using the SuppressUnderflow() and SuppressOverflow() 
//            functions.
//
// 2010-11-11 Modified the above entry
// John Annand 19/02/14 Modify for TSIS1100 interface class
//
////////////////////////////////////////////////////
#ifndef _TCaen792_H_
#define _TCaen792_H_

#include "TObject.h"

class TSIS1100;
class TCaen792: public TObject{

private:
  TSIS1100* fSIS;
  UInt_t fbase;                  // Base address for io
  UInt_t hwaddress;                      // The hwaddress
  UShort_t debug;                        // Debug flag
public:
  TCaen792( TSIS1100*, UInt_t, UShort_t = 0);  // hw address, debugflag
  virtual ~TCaen792();

  // Generic write/read access to the 16 bit mem area
  void SetShort( UShort_t address, UShort_t value);
  UShort_t GetShort( UShort_t address);
  void DataReset() { SetShort(0x1032,0x4); SetShort(0x1034,0x4);}
  void SoftwareReset() { SetShort(0x1006,0x80); SetShort(0x1008,0x80);}

  void SuppressUnderflow( Bool_t flag, UShort_t thres = 16);
  void SuppressOverflow( Bool_t flag);
  void SetUnderflowThreshold( UShort_t ch, UShort_t thres);
  void SetUnderflowThresholds( UShort_t thres);
  void DeactivateChannels( UInt_t bitmask);  // mask off channels according to the bitpattern

  void PrintIped();
  void SetIped( UShort_t iped);

  UShort_t BufferEmpty();                             // is the data buffer empty
  UShort_t Busy();                                    // is the module busy
  UShort_t GetMultiplicity();                         // the multiplicity of the event
  void GetDatum( UShort_t *channel, UShort_t *value); // get the next datum (ch,val) of the event
  UShort_t IsEOB();                                   // have we reached the End Of Block of 
                                                      // of the current event

  UShort_t GetData( UShort_t *ch, UShort_t *val);

  void PrintThresholds();
  void Info();
  void PrintStatus();

  ClassDef( TCaen792, 0 )
};

#endif /* _TCaen792_H_ */




