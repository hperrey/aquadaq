///////////////////////////////////////////////////////////////////
//
// Caen 1724 100 MHz FADC
//
///////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "TCaen1724.h"

ClassImp( TCaen1724 )

const char description[] = "Caen 1724 100 MHz FADC";
const char memdev[] = "/dev/vmedrv32d32";
const UShort_t CAEN1724SIZE = 0xffff;

/////////////////////////////////////////////////////////////////////////
TCaen1724::TCaen1724( UInt_t BASE, UShort_t debugflag)
{
  hwaddress = BASE;
  debug = debugflag;      
  Init( BASE);
}
/////////////////////////////////////////////////////////////////////////
TCaen1724::~TCaen1724( )
{
  munmap( (char *)fbase, CAEN1724SIZE);
  close( ffd);
}
/////////////////////////////////////////////////////////////////////////
void TCaen1724::SetShort( UShort_t address, UShort_t value)
{
  *(fbase16 + address/2) = value;
  return;
}
UShort_t TCaen1724::GetShort( UShort_t address)
{
 return ( *(fbase16 + address/2) );  
}
UInt_t TCaen1724::GetLong( UShort_t address)
{
 return ( *(fbase32 + address/4) );  
}
void TCaen1724::SetLong( UShort_t address, UInt_t value)
{
  *(fbase32 + address/4) = value;
  return;
}
/////////////////////////////////////////////////////////////////////////
void TCaen1724::Init( UInt_t BASE)
{
  if (  (ffd = open( memdev, O_RDWR | O_SYNC)) == -1) 
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("open() failed: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }

  if ((fbase = (volatile void *)mmap(NULL, CAEN1724SIZE, PROT_WRITE, MAP_SHARED, ffd, BASE)) == MAP_FAILED)
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("mmap() falied: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }

  if ( fbase == (void *)-1 )
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      exit(-1);
    }

  fbase16 = (volatile UShort_t *)((char *)fbase);
  fbase32 = (volatile UInt_t *)((char *)fbase);  

  if ( debug > 1)
    {
      fprintf( stderr, "fbase at %p \n", fbase);
      Info();
    }

  SwReset();
  SwClear();

  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1724::Info()
{
  for ( int i = 0; i < 1020; i++)
    SwTrigg();
  return;
}
/////////////////////////////////////////////////////////////////////////
void TCaen1724::Start()
{
  SetLong( 0x8100, 0x4 | GetLong( 0x8100) );
  return;
}
/////////////////////////////////////////////////////////////////////////
void TCaen1724::Stop()
{
  SetLong( 0x8100, ~0x4 & GetLong( 0x8100) );
  return;
}
/////////////////////////////////////////////////////////////////////////
UShort_t TCaen1724::GetData( UShort_t *ch, UShort_t *val)
{
  UShort_t pattern;
  UShort_t boardId;
  UShort_t eventSize;
  UInt_t eventCounter;
  UShort_t channelMask;
  UInt_t triggerTimeTag;

  UInt_t datum;


  datum = GetLong( 0x0);
  eventSize = 0xffff & datum; 
  pattern = 0x000f & datum >> 28;
  printf("eventSize %d, pattern %d\n", eventSize, pattern);

  datum = GetLong( 0x0);
  channelMask = 0x00ff & datum; 
  boardId = 0x001f & datum >> 27;
  printf("channelMask %d, boardId %d\n", channelMask, boardId);

  datum = GetLong( 0x0);
  eventCounter = 0x00ffffff & datum; 
  printf("eventCounter %d\n", eventCounter);

  datum = GetLong( 0x0);
  triggerTimeTag = datum; 
  printf("triggerTimeTag %d\n", triggerTimeTag);


  UShort_t reads = (eventSize - 4)/2;
  UShort_t readsPerChannel = reads/8;
  UShort_t channel, timeInterval;

  printf("reads %d, readsPerChannel %d\n", reads, readsPerChannel);

  for ( channel = 0; channel < 8; channel++)
    {
      printf("channel %d\n", channel);
      for ( timeInterval = 0; timeInterval < readsPerChannel; timeInterval++)
	{
	  datum = GetLong( 0x0);
	  *ch++  = channel;
	  *val++ = 0x00003fff & datum;
	  *ch++  = channel;
	  *val++ = 0x00003fff & (datum >> 16);
	}

    }

  return 0;
}
/////////////////////////////////////////////////////////////////////////
void TCaen1724::bitprint( UInt_t buffer)
{
  int i = 31;
  while( i >= 0)
    {
      if ( (buffer >> i) & 0x00000001 )
    	printf("1");
      else
	printf("0");
      if ( (i % 8 == 0) && (i != 0))
	printf("|");
      i--;
    }
  printf("\n");
}
//////////////////////////////////////////////////////////////////////////


