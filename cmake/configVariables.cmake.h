#pragma once

// CMake uses config.cmake.h to generate config.h within the build folder.
#ifndef AQUADAQ_CONFIG_H
#define AQUADAQ_CONFIG_H

#define PACKAGE_NAME "@CMAKE_PROJECT_NAME@"
#define PACKAGE_VERSION "@AQUADAQ_FULL_VERSION@"
#define PACKAGE_STRING "@CMAKE_PROJECT_NAME@ @AQUADAQ_FULL_VERSION@"

#define PACKAGE_SOURCE_DIR "@PROJECT_SOURCE_DIR@"
#define PACKAGE_INSTALL_DIR "@CMAKE_INSTALL_PREFIX@"

#define DATA_PATH "@DATA_PATH@"

#endif
