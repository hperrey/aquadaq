#ifndef _TSAL4417_H_
#define _TSAL4417_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"


class TSAL4417 : public TObject
{
private:
  TCamacBranch *fCBD;
  const int fCrate;             // Crate number
  const int fSlot;              // Slot number
  const int fChannels;          // Number of channels

  volatile UShort_t *pClear;     // Pointer to clear command
  volatile UShort_t *pReadClear; // Pointer to the first channel to be read 
  volatile UShort_t *pRead;      // Pointer to the first channel to be read 

public:
  TSAL4417(TCamacCrate *C, int n, int channels = 16);
  virtual ~TSAL4417();

  void PrintAll();                   // Print all channels

  UShort_t ReadClearArray( UInt_t *arr);
  UShort_t ReadArray( UInt_t *arr);

  ClassDef( TSAL4417, 0);

};


#endif /* _TSAL4417_H_ */





