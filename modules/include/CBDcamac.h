/*------------------------------------------------------------------------------
  File:    CBDcamac.h
  Created: 1998-11-20
  Author:  Dahn Nilsson
  Function: 
  Access the CAMAC branch through the CES CBD8210
  Comments:
  Loosely based on Hendrik Ruijter's qcamac.
  Refer to the CES CBD8210 Manual.
  Version: 
  1.0 1998-11-20 Implementation.
------------------------------------------------------------------------------*/
#ifndef CBDCAMAC_H
#define CBDCAMAC_H

typedef unsigned short uint16;
typedef unsigned int uint32;

#define CBDBRANCHPOS	19	/* b2 b1 b0 */
#define CBDCRATEPOS	16	/* c2 c1 c0 */
#define CBDSTATIONPOS	11	/* n4 n3 n2 n1 n0 */
#define CBDSUBADDRPOS	7	/* a3 a2 a1 a0 */
#define CBDFUNCPOS	2	/* f4 f3 f2 f1 f0 */
#define CBDDATASIZEPOS	1	/* d0 */

/*
 *	Adress space for CAMAC
 */
#define CBDBASE  0xf0800000      /* Standard access on MVME 167 */
#define CBDBRANCH( b ) (CBDBASE + ( b << CBDBRANCHPOS ) )
#define CBDSIZE ( 1 << CBDBRANCHPOS )

#define CBD16BIT	0X0002
#define CBD24BIT	0X0000

#define DEFAULTBRANCH 1

#define CBDOffset16( c, n, a, f)			   \
( ((c << CBDCRATEPOS)   | (n << CBDSTATIONPOS) |            \
   (a << CBDSUBADDRPOS) | (f << CBDFUNCPOS)  | CBD16BIT )>>1)

#define CBDOffset( c, n, a, f, t)			   \
( ((c << CBDCRATEPOS)   | (n << CBDSTATIONPOS) |            \
   (a << CBDSUBADDRPOS) | (f << CBDFUNCPOS)  | (t<<1) ))

#define CBDOffset24( c, n, a, f)			   \
( (c << CBDCRATEPOS)   | (n << CBDSTATIONPOS) |            \
  (a << CBDSUBADDRPOS) | (f << CBDFUNCPOS) )

#define CBDRead16(n, a) ((uint16)*n[a])
#define CBDWrite16(n, a, val) (*n[a]= (uint16)val)

#define CBDRead24(n, a) ((uint32)*n[a])
#define CBDWrite24(n, a, val) (*n[a]= (uint32)val)

typedef volatile uint16 *CBD16Module;
typedef volatile uint32 *CBD24Module;

/*
 *	Operations for Q-response

CBD16Module CBD_CSR, CBD_IFR;

#define QCBDPOS		0X1000
#define CBD_Q() (*CBD_CSR && QCBDPOS)
*/

/*
 *      Prototypes
 */
#ifdef __cplusplus
extern "C" int OpenCBDBranch( int branch );
extern "C" uint16 *MapCBDModule16( int crate, int slot, int substation, int func);
extern "C" uint32 *MapCBDModule24( int crate, int slot, int substation, int func);
extern "C" int Camac24Write(int crate, int  slot, int substation, int func, int val);
extern "C" int Camac16Write(int crate, int  slot, int substation, int func, int val);
extern "C" int Camac24Read(int crate, int  slot, int substation, int func);
extern "C" int Camac16Read(int crate, int  slot, int substation, int func);

extern "C" int CloseCBDBranch();
#else
int OpenCBDBranch( int branch );
uint16 *MapCBDModule16( int crate, int slot, int substation, int func);
uint32 *MapCBDModule24( int crate, int slot, int substation, int func);
int Camac24Write(int crate, int  slot, int substation, int func, int val);
int Camac16Write(int crate, int  slot, int substation, int func, int val);
int Camac24Read(int crate, int  slot, int substation, int func);
int Camac16Read(int crate, int  slot, int substation, int func);

int CloseCBDBranch();

#endif /* __cplusplus */
#endif /* CBDCAMAC_H */

