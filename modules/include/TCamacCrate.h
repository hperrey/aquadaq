/////////////////////////////////////////////////////
// 
// Camac crate class
//
// Dahn Nilsson 1999-03-02
// John Annand 19/02/14 Modify for TSIS1100 interface class
////////////////////////////////////////////////////
#ifndef _TCamacCrate_H_
#define _TCamacCrate_H_

#include "TObject.h"
#include "TCamacBranch.h"

class TCamacCrate: public TObject{

private:

  Int_t fCrate;             // Crate number
  TCamacBranch *fCBD;

  UInt_t fClear;     // Clear crate (C)
  UInt_t fReset;     // Reset crate (Z)
  UInt_t fTestInhibit;   // Test dataway inhibit
  UInt_t fClearInhibit;  // Clear dataway inhibit
  UInt_t fSetInhibit;    // Set dataway inhibit

public:
  TCamacCrate(TCamacBranch *cbd, const int c = 1);
  virtual ~TCamacCrate();


  Bool_t Q() {return fCBD->Q();}
  Bool_t X() {return fCBD->X();}

  void Clear() {fCBD->Read(fClear);}
  void Reset() {fCBD->Read(fReset);}
  Bool_t Inhibit();             // Test inhibit
  Bool_t Inhibit( Bool_t b);    // Set/Clear inhibit

  Int_t Crate() { return fCrate; }

  TCamacBranch *GetCBD() {return fCBD;}
  ClassDef( TCamacCrate, 0 )  // Camac Branch driver 
};

#endif /* _TCamacCrate_H_ */
