///////////////////////////////////////////////////////////////////
//
// Caen 534 Flash ADC
// Magnus Lundin 2001-01-28 (based on TStruckDL515)
//
///////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "TCaen534.h"

ClassImp( TCaen534 )

static int init = 0;

#ifdef SIS
const char memdev[] = "/dev/sis110032d16";
#endif
#ifdef SBS
const char memdev[] = "/dev/vmedrv32d16";
#endif

TCaen534::TCaen534(UInt_t base, int depth, int channels)
{
  hwaddress = base;
  fDepth = depth;
  fChannels = channels;

  Init();
}

TCaen534::~TCaen534( )
{
  if ( --init )
    return;

  munmap( (char *)fbase, CAEN534SIZE);
  close( fdevmem );
}

void TCaen534::Init( )
{
  if( init++ )
    return;

  if (  (fdevmem = open( memdev, O_RDWR | O_SYNC)) == -1) 
    {
      perror("open /dev/mem");
      return;
    }

  fbase = (volatile void *)mmap(NULL, CAEN534SIZE, PROT_WRITE, MAP_SHARED, fdevmem,
				hwaddress);

  if ( fbase == (void *)-1 )
    {
      perror("Error mmapping");
      return;
    }

  fData         = (volatile UInt_t *)fbase;
   
  fEventCounter = (volatile UShort_t *)((char *)fbase + 0x0c);
  fStop         = (volatile UShort_t *)((char *)fbase + 0x0c);

  fReleaseB     = (volatile UShort_t *)((char *)fbase + 0x0a);
  fLatchBStat   = (volatile UShort_t *)((char *)fbase + 0x0a);
  fReleaseA     = (volatile UShort_t *)((char *)fbase + 0x08);
  fLatchAStat   = (volatile UShort_t *)((char *)fbase + 0x08);

  fMemMode      = (volatile UShort_t *)((char *)fbase + 0x06);
  fRegMode      = (volatile UShort_t *)((char *)fbase + 0x04);
  fDAC          = (volatile UShort_t *)((char *)fbase + 0x02);
  fReset        = (volatile UShort_t *)((char *)fbase + 0x00);

  Reset();
#if DEBUG
  PrintBlockStatus();
#endif
  ReleaseA();
  ReleaseB();
  Stop();
}


int TCaen534::AReadable()          
{
  // return 1 if A is idle and full else 0

  SetRegMode();

  UShort_t stat = GetLatchAStat();

  if( (stat & AIDLE) && (stat & AFULL) )
    return 1;

  return 0;
}

int TCaen534::BReadable()          
{
  // return 1 if B is idle and full else 0

  SetRegMode();

  UShort_t stat = GetLatchBStat();

  if( (stat & BIDLE) && (stat & BFULL) )
    return 1;

  return 0;
}

int TCaen534::Load()
{

  volatile UInt_t *pFirst = 0;          // pointer to the first memory address 
  volatile UInt_t *pLast  = 0;          // pointer to the last memory address 
  volatile UInt_t *pStop  = 0;          // pointer to the stop memory address   
  volatile UInt_t *p;
  UInt_t depth_counter = 0;             // keeps track of how far we have read


#if DEBUG
  PrintBlockStatus();
#endif
  pStop = GetStopAddress();
  
  //
  // Determine first and last latch addresses  (ONLY VALID FOR THE FIRST 4 CHANNELS)
  // See manual for addresses
  //
  if( AReadable() && !BReadable())
    {
      pFirst = fData + 0x0000;
      pLast  = fData + (0x3ffc >> 2);
    }
  else if( !AReadable() && BReadable())
    {
      pFirst = fData + (0x8000 >> 2);    
      pLast  = fData + (0xbffc >> 2); 
    }
  else if( AReadable() && BReadable())
    {
      fprintf( stderr, "\n \n In file %s at line %d \n", __FILE__, __LINE__);     
      fprintf( stderr, "A & B readable \n \n ");
      // ala ctor 
      Reset();
      PrintBlockStatus();
      ReleaseA();
      ReleaseB();
      Stop();
      return -1;      
    }
  else if( !AReadable() && !BReadable())
    {
      fprintf( stderr, "\n \n In file %s at line %d \n", __FILE__, __LINE__);     
      fprintf( stderr, "!A & !B readable \n \n ");
      // ala ctor 
      Reset();
      PrintBlockStatus();
      ReleaseA();
      ReleaseB();
      Stop();
      return -1;            
    }

#if DEBUG
  printf("first %p \n", pFirst);
  printf("stop  %p \n", pStop);
  printf("last  %p \n", pLast);
#endif

  // Check that pStop falls between first and last
  if(!((pLast >= pStop) && (pStop >= pFirst)))
    {
      fprintf( stderr, "\n \n In file %s at line %d \n", __FILE__, __LINE__);     
      fprintf( stderr, "pStop was not between pFirst and pLast \n \n ");
      // ala ctor 
      Reset();
      PrintBlockStatus();
      ReleaseA();
      ReleaseB();
      Stop();
      return -1;
    }

  SetMemMode();

  // Read to the beginning of the block or to the desired depth
  for(p = pStop; (p >= pFirst) && (depth_counter < GetDepth());)
    {
#if DEBUG
      printf("1 %p \n", p);
#endif
      fStore[depth_counter++] = *p--;
    }

  // Read from the end of the block or to the location of pStop + 1 or to the desired depth
  for(p = pLast; (p > pStop) && (depth_counter < GetDepth());)
    {
#if DEBUG
      printf("2 %p \n", p);
#endif
      fStore[depth_counter++] = *p--;
    }

#if DEBUG
  printf("depth_counter  %d \n", depth_counter);
#endif

  ReleaseA();
  ReleaseB();

  return 0;
}

Int_t TCaen534::ReadArray( UShort_t *Array, UInt_t Channel, UInt_t depth)
{
  // Read an array of depth values into Array
  // from channel starting on 0.
  // Have to have called Load() first.
  
  UInt_t i, j;
  UChar_t *fRead = (UChar_t *)fStore;

  if( Channel < 0 || Channel > 3)
    {
      fprintf( stderr, "In file %s at line %d \n", __FILE__, __LINE__);      
      fprintf( stderr, "Invalid channel No (%d). Valid : 0-3 \n", Channel);
      return 0;
    }
  
  // This doesnt make sense according to the manual, but ...
  Channel = 3 - Channel;

  if(depth > GetDepth())
    depth = GetDepth();

  for( i = j = 0; i < depth; i++, j += 4)
    *Array++ = *(fRead + j + Channel);

  return i;
}

void TCaen534::Test( UInt_t depth)
{
  const UInt_t dimension = 500;

  //  Stop();

  Load();

  UShort_t Array0[dimension], Array1[dimension], Array2[dimension], Array3[dimension];

  ReadArray(Array0,0,GetDepth());
  ReadArray(Array1,1,GetDepth());
  ReadArray(Array2,2,GetDepth());
  ReadArray(Array3,3,GetDepth());
 
  if (depth > dimension)
    {
      depth = dimension;
      printf("Depth was changed to %d \n", depth);
    }

  for (UInt_t i = 0; i < depth; i++)
    printf("%d \t %d \t %d \t %d \t %d \n", i, Array0[i], Array1[i], Array2[i], Array3[i]);
}


void TCaen534::PrintBlockStatus()  
{
  //
  // Print status for block A and B
  //

  SetRegMode();

  // Block A 
  UShort_t statA = GetLatchAStat();

  printf("StatA    0x%x \n", statA);

  if (statA & AIDLE)
      printf("Block A is idle \n");
  else
      printf("Block A is active \n");

  if (statA & AFULL)
      printf("Block A is full \n");
  else
      printf("Block A is free \n");


  // Block B 
  UShort_t statB = GetLatchBStat();

  printf("StatB    0x%x \n", statB);

  if (statB & BIDLE)
      printf("Block B is idle \n");
  else
      printf("Block B is active \n");

  if (statB & BFULL)
      printf("Block B is full \n");
  else
      printf("Block B is free \n");

}

UInt_t *TCaen534::GetStopAddress()
{
  //
  // Determine which block to read from and return a poiner to the stop address
  //

  SetRegMode();

  UShort_t statA = GetLatchAStat();
  UShort_t statB = GetLatchBStat();
  UShort_t stat  = 0;

  // Return an error if both are ready to read from
  if( AReadable() && BReadable() )
    {
      perror("Both A and B are idle and full \n");
      return NULL; 
    }
  
  if( AReadable() )
    {
#if DEBUG
      printf("Read from A \n");
#endif
      stat = statA;
    }

  if( BReadable() )
    {
#if DEBUG
      printf("Read from B \n");
#endif
      stat = statB;
    }

  // Get the stop latch address
  stat = stat & 0xfff;
#if DEBUG
  printf("stat  %x \n", stat);  
  printf("fData  %p \n", fData);
#endif

  // Reading the manual makes you think the address should be masked
  // However, it doesnt work
  // UInt_t stopLatchAddress = ((UInt_t )fData) & 0xffff0000;
  // Makes more sense to have do like below instead
  UInt_t stopLatchAddress = ((UInt_t )(size_t)fData);

  // Stop address depends on A and B
  if( AReadable() == 1)
    stopLatchAddress = stopLatchAddress + (stat << 2);
  else
    stopLatchAddress = stopLatchAddress + 0x8000 + (stat << 2);

  if ( GetChannels() > 4)
    {
      fprintf( stderr, "In file %s at line %d \n", __FILE__, __LINE__);
      fprintf( stderr, "Channels in the range 0-3 \n");            
      stopLatchAddress = stopLatchAddress + 0x00004000;
      return NULL;
    }

#if DEBUG
  printf("true stop %x \n", stopLatchAddress);
#endif

  return (UInt_t *)stopLatchAddress;

}

