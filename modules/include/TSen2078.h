/////////////////////////////////////////////////////
// 
// Sen 2078 real time clock (based on TCamacUnit)
//
// Dahn Nilsson 1999-03-02
// Magnus Lundin 2005-09-24
//
// Load(), external start, Read()
//
////////////////////////////////////////////////////
#ifndef _TSen2078_H_
#define _TSen2078_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

class TSen2078: public TObject{

private:

  TCamacBranch *fCBD;
  const int fCrate;             // Crate number
  const int fSlot;              // Slot number

  volatile UShort_t *pReadLW;   // The Low Word
  volatile UShort_t *pReadHW;   // The High Word
  volatile UShort_t *pWrite;
  volatile UShort_t *enableCounting;
  volatile UShort_t *disablePredivider;  
  volatile UShort_t *presetLW;
  volatile UShort_t *presetHW;

  volatile Short_t *fClear; // Clear module
  volatile Short_t *fTest; // Test module

public:
  TSen2078(TCamacBranch *cbd, int c, int n);
  TSen2078(TCamacCrate *C, int n);
  virtual ~TSen2078();

  void Load();
  UInt_t Read();
  void Clear() {if (*fClear) ;}

  Bool_t Q() {return fCBD->Q();} // Q response
  Bool_t X() {return fCBD->X();} // X response

  void Test();
  void Print();
  
  ClassDef( TSen2078, 0)  // Camac unit
};

#endif /* _TSen2078_H_ */
