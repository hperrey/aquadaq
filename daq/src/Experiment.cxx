////////////////////////////////////////////////////////////////////////
//#include <sys/types.h>
//#include "../sis1100-2.13-8/sis3100_calls/sis3100_vme_calls.h"
#include "TDirectory.h"

#include "Experiment.h"

#include <time.h>

ClassImp(Experiment)

static int init=0;

//________________________________________________________________________
Experiment::Experiment()
  :
  //
  // X
  //
  fSIS(),
  CBD(&fSIS,0),
  fcrate1( &CBD, 1),
  //
  //fvt0( &fSIS,0x0a470000, 0 ), // 
  fvt1( &fSIS,0x0a480000, 0 ),   // V775
  fvt2( &fSIS, 0xa1030000, 0, 4, 1), //V1190
  //fvtt( &fSIS, 0xa1100000, 0, 4, 1),
  fvq0( &fSIS, 0xa1010000, 0 ), // V792
  fvq1( &fSIS, 0xa1000000, 0 ), // V792
  //
  // crate 1


  fc1_1(&fcrate1, 1),      // 2259 B VDC
  fc1_2(&fcrate1, 2),      // 2259 B VDC
  fc1_3(&fcrate1, 3),      // 2259 B VDC

  fc1_4(&fcrate1, 4),      // 2228 TDC
  fc1_5(&fcrate1, 5),      // 2228 TDC
  fc1_9( &fcrate1, 9),     // 4208 TDC

  //  fc1_21( &fcrate1, 21),   // 4208 TDC
  // fc1_20( &fcrate1, 20),   // 2259 VDC

  //
  // Scaler
  //
  fvs0(&fSIS, 0x0a010000, 1), //ok
  fvs1(&fSIS, 0x0a020000, 1),  //ok
  fc1_8( &fcrate1, 8)
{
  if( init++ )
    return;
  //fvq0.DeactivateChannels(0xfff80000); // keep channels 0-18
  //fvq0. SetIped(120); // set IPed
  fvt2.SetWindowWidthNS( 1000.0);
  fvt2.SetWindowOffsetNS( -200.0);
  //fvtt.SetWindowWidthNS( 750.0);
  //fvtt.SetWindowOffsetNS( -50);

  printf("About to clear CAMAC crates\n");
  fcrate1.Inhibit(0);
  fcrate1.Clear();
  printf("Done clearing CAMAC crates\n");

 
  // Reset LATCH
  CBD.ToggleAck4();
  // Let the changes take effect
  sleep(1);
  // explicitly clear the VME scalers to avoid offsets
  fvs0.SoftwareClear();
  fvs1.SoftwareClear();
}
//________________________________________________________________________
Experiment::~Experiment()
{
  init--;
}


//------------------------------------------------------------------------
Int_t Experiment::ReadXEvent( XEvent *xev)
{
  static UInt_t evno = 0;
  UShort_t channels = 0;
  UShort_t iData, nData;
  UShort_t ch[256];
  UShort_t val[32];
  UInt_t lval[256];
  int i;

  // Wait 1 second for interrupts
  if ( CBD.Poll(1) != 0)
    return -1;

  xev->SetEventNumber( ++evno);
  //
  // focal plane tdcs
  //
  // Read out vme tdc0
  // Read out vme tdc tagger (multi)

  /*
  iData = 0;
  nData = fvtt.GetData( ch, lval); 
  channels = channels + nData;
  while( iData < nData) 
    {
      xev->AddTagg( ch[iData], lval[iData]);
      iData++;
    }

  */
  // Read out vme tdc 2 (multi)
  iData = 0;
  nData = fvt2.GetData( ch, lval);
  channels = channels + nData;
  while( iData < nData) 
    {
      xev->AddVtdc( ch[iData], lval[iData]);
      iData++;
    }
  /*  
  // Read out vme tdc 1
  iData = 0;
  nData = fvt1.GetData( ch, lval);
  channels = channels + nData;
  while( iData < nData) 
    {
      xev->AddVtdc( ch[iData], lval[iData]);
      iData++;
    }
  */

  // Read out vme qdc 0 
  iData = 0;
  nData = fvq0.GetData( ch, val); 
  channels = channels + nData;
  while( iData < nData) 
    {

      xev->AddVqdc( ch[iData], val[iData]);
      iData++;
    }

  // Read out vme qdc 1 
  iData = 0;
  nData = fvq1.GetData( ch, val); 
  channels = channels + nData;
  while( iData < nData) 
    {
      xev->AddVqdc(32+ch[iData], val[iData]);
      iData++;
    }

  // Read 2259 ADC
  for(i=0; i<12; i++)
    xev->Setc1_1(i, fc1_1[i]);
  channels += 12;
  for(i=0; i<12; i++)
    xev->Setc1_2(i, fc1_2[i]);
  channels += 12;
  for(i=0; i<12; i++)
    xev->Setc1_3(i, fc1_3[i]);
  channels += 12;
  // Read 2228 TDC
  for(i=0; i<8; i++)
    xev->Setc1_4(i, fc1_4[i]);
  channels += 8;
  for(i=0; i<8; i++)
    xev->Setc1_5(i, fc1_5[i]);
  channels += 8;
  // Read 4208 TDC
  for(i=0; i<8; i++)
    xev->Setc1_9(i, fc1_9[i]);
  channels += 8;
  
  //Reset LATCH
  CBD.ToggleAck4();

  return channels;
}
//------------------------------------------------------------------------
Int_t Experiment::ReadScalerEvent(ScalerEvent *sev)
{
  static UInt_t evno = 0;
  UShort_t i, mult, channels = 0;
  UInt_t val[32];
  UShort_t ch[32];

  sev->SetEventNumber( ++evno );
  
  mult = fvs0.GetData( ch, val);
  for ( i = 0; i < mult; i++)
    sev->Setvs0( ch[i], val[i]);
  channels += mult;

  mult = fvs1.GetData( ch, val);
  for ( i = 0; i < mult; i++)
    sev->Setvs1( ch[i], val[i]);
  channels += mult;

  // explicitly clear the VME scalers after readout
  fvs0.SoftwareClear();
  fvs1.SoftwareClear();

  //
  // X-arm scalers
  //
  fc1_8.ReadClearArray( val);  
  for( i = 0; i < 32; i++)
    sev->Setc1_8( i, val[i]&0xffffff);
  channels += 32;
  //
  return channels;
}

