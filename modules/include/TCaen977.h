/////////////////////////////////////////////////////
// 
// Caen 977 
//
// Magnus Lundin 
//
////////////////////////////////////////////////////
#ifndef _TCaen977_H_
#define _TCaen977_H_

#include "TObject.h"


class TCaen977: public TObject{

private:

  int ffd;                               // File descriptor
  volatile void *fbase;                  // Base address for io
  volatile UShort_t *fbase16;            // 16 bit access
  volatile UInt_t   *fbase32;            // 32 bit access
  UInt_t hwaddress;                      // The hwaddress
  UShort_t debug;                        // Debug flag

public:
  TCaen977( UInt_t BASE, UShort_t debugflag = 0);  // hw address
  virtual ~TCaen977();

  // Generic write/read access to the 16 bit mem area
  void SetShort( UShort_t address, UShort_t value);
  UShort_t GetShort( UShort_t address);
  
  void Init( UInt_t BASE);

  void SoftwareReset(){  SetShort( 0x002e,0x0000);};
  void ClearBitpattern(){ SetShort( 0x0010,0x0000);};

  void GatedInputMode(){ SetShort( 0x000c,0xffff);  SetShort( 0x0028,0x0001);};
  void OutputMode(){  SetShort( 0x000c,0x0000);};

  UShort_t GetBitpatternSH();
  UShort_t GetBitpatternMH();
  UShort_t SetBitpattern( UShort_t bp);
  UShort_t PollBit( UShort_t bit, UShort_t sec);

  UShort_t AccessTest();  // Do some write and reads to memory
  void PrintStatus();


  ClassDef( TCaen977, 0 )
};

#endif /* _TCaen977_H_ */




