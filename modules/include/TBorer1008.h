////////////////////////////////////////////////////////////////////////////
// Borer CAMAC modules 
// 
////////////////////////////////////////////////////////////////////////////

#ifndef _TBorer_H_
#define _TBorer_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

class TBorer1008: public TObject{
////////////////////////////////////////////////////////////////////////////
// 
// Borer 1008  (CAMAC)
// Borer 1008 Dual preset counter/timer
// 
//  35 MHz guaranteed rate.
//  Two 24-bit counters.
//  Intermediate readout possible.
//
// Dahn Nilsson 2000-02-13
///////////////////////////////////////////////////////////////////////////////////

private:

  static const int fNoChannels = 2; // Number of channels

  TCamacBranch *fCBD;
  Int_t fCrate;			// Crate number
  Int_t fSlot;			// Slot number

  volatile Int_t *pRead[fNoChannels];	// Read addresses
  volatile Int_t *pWrite[fNoChannels]; // Write addresses
  
  volatile Short_t *pClear[fNoChannels]; // Clear module
  volatile Short_t *pStart[fNoChannels]; // Start the counter

  void InitUnit();		// Initialize stuff

  volatile Short_t *func(int func, int address) // Get address
    {return (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, address, func); }

public:

  TBorer1008(TCamacBranch *cbd, int c, int n);
  TBorer1008(TCamacCrate *C, int n);
  virtual ~TBorer1008();

  Int_t& operator[](int n){return *(Int_t *)pRead[n];};
  Int_t ReadArray( Int_t *Array, Int_t Count);

  Bool_t Q() {return fCBD->Q();} // Q response
  Bool_t X() {return fCBD->X();} // X response

  Bool_t Write(int val, int n){ *pWrite[n] = val; return Q();};	// Load the buffers

  void Clear(int n) { if ( *pClear[n] ) ; } // Clear counter n
  void Start(int n) { if ( *pStart[n] ) ; } // Start counter n

  void Test(int l=3);

  void Print();
  
  ClassDef( TBorer1008, 0)  // Camac unit
};

#endif // _TBorer_H_
