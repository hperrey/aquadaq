//////////////////////////////////////////////////////////////////////
//
// Implementation of the Phillips 7132 32-channel scalers
// Note the extra t = 0 in the call to cnaf to specify
// long words.
//
// Dahn Nilsson
//
// Modified 061123
// Two 16 bit calls are made for each scaler channel
// 
//
//////////////////////////////////////////////////////////////////////
#include "TPhillips7132.h"

ClassImp( TPhillips7132);  

TPhillips7132::TPhillips7132(TCamacBranch *cbd, UShort_t c, UShort_t n, UShort_t ch ):
fCBD( cbd ), fCrate(c), fSlot(n)
{
 
  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fNoChannels = ch;

  selBank = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 1, 17);
  fClear =  (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 4, 11);

  for( int i = 0; i < 16; i++)
    pRead[i] = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, i, 0, 1); // T = 1
 
}

TPhillips7132::TPhillips7132(TCamacCrate *C, UShort_t n, UShort_t ch ):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{
 
  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fNoChannels = ch;

  selBank = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 1, 17);
  fClear =  (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 4, 11);

  for( int i = 0; i < 16; i++)
    pRead[i] = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, i, 0, 1); // T = 1
 
}

TPhillips7132::~TPhillips7132( )
{
}

void TPhillips7132::ReadArray( UInt_t *arr)
{
  *selBank = 0;

  for( int i = 0; i < 16; i++)
    {
      *arr    =  *( volatile UShort_t *)(pRead[i] - 1) << 16;
      *arr++ |= *( volatile UShort_t *)(pRead[i]);
    }

  *selBank = 1;

  for( int i = 0; i < 16; i++)
    {
      *arr    =  *( volatile UShort_t *)(pRead[i] - 1) << 16;
      *arr++ |= *( volatile UShort_t *)(pRead[i]);
    }
}

UInt_t& TPhillips7132::operator[](UShort_t n )
{
  static int bank = 0;
  static UInt_t val = 0;

  if(  n > 15 && bank == 0 )
    *selBank = bank = 1;
  else if( n < 16 && bank == 1 )
    *selBank = bank = 0;

  if ( n > 31)
    {
      printf("n out of range for Phillips 7132\n");
      return val;
    }
  if ( n > 15)
    n = n - 16;

  val  = *( volatile UShort_t *)(pRead[n] - 1) << 16;
  val |= *( volatile UShort_t *)(pRead[n]);  

  return val;
}

int TPhillips7132::Test( UShort_t t)
{
  // Test register only hold 8 bits!

  int fail = 1, c, count;
  volatile Int_t *testreg = (volatile Int_t *)fCBD->cnaf( fCrate, fSlot, 2, 17, 0);

  if( t == 3 )
    printf("Running selftest. Make sure crate is inhibited!\n");

  Clear();

  count = 0xff;

  *testreg = count;

  for( int i = 0; i < 256*256; i++, count += 0xff)
    {
      if( *(volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 25) ) // Start test
	;

      c = operator[](0);  // Dummy to allow for count time
      for( int i = 0; i < fNoChannels; i++)
	{
	  c = operator[](i);
	  if(  c != count )
	    {
	      fprintf( stderr, 
		       "Channel %d of scaler in slot %d crate %d failed selftest! Count = 0x%x should be 0x%x!\n",
		       i, fSlot, fCrate, c, count);
	      fail = 0;
	    }
	}
  
    }

  if(  t == 3 )
    PrintAll();

  if( ! fail && t == 3 )
    printf( "Scaler passed test!\n");

  return fail;
}

void  TPhillips7132::PrintAll()
{
  UInt_t arr[32];

  ReadArray( arr);  
  for( int i = 0; i < fNoChannels; i++)
    printf("%d\t%8d\t0x%08x\n", i, arr[i], arr[i]);

}



