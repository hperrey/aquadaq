// John Annand  19th Feb 2014  Modify for SIS1100

#include "TLRS2228.h"

ClassImp( TLRS2228 )

TLRS2228::TLRS2228(TCamacBranch *cbd, int c, int n): 
fCBD( cbd ), fCrate(c), fSlot(n)
{
  InitUnit();
}

TLRS2228::TLRS2228(TCamacCrate *C, int n):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{
  InitUnit();
}

TLRS2228::~TLRS2228( )
{
}

void TLRS2228::InitUnit()
{
  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fClear = fCBD->cnaf( fCrate, fSlot, 0, 9);
  fTest = fCBD->cnaf( fCrate, fSlot, 0, 25);

  for( int i = 0; i < fNoChannels ; i++)
    {
      pRead[i] = fCBD->cnaf( fCrate, fSlot, i, 2);
      pWrite[i] = fCBD->cnaf( fCrate, fSlot, i, 17);
    }
}

Int_t TLRS2228::ReadArray( Short_t *Array, Int_t count)
{
  // Read an array of count values into Array
  // from channel starting on 0.
  
  int i;

  for( i = 0; i < count; i++)
    *Array++ = fCBD->Read(pRead[i]);

  return i;
}

void TLRS2228::Test(int l)
{
  volatile Short_t dum;

  dum = fCBD->Read(fTest);
}

void TLRS2228::Print( )
{
  for( int i = 0; i < fNoChannels; i++)
    printf( "Channel: %d, value: %d\n", i, fCBD->Read(pRead[i]));
}
