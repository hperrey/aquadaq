////////////////////////////////////////////////////////////////////////////
// 
// LeCroy 4413 high-speed discrimintaor (CAMAC)
// 16 channels
// 
// 061206 Magnus Lundin
// John Annand 19/02/14 Modify for TSIS1100 interface class
//
///////////////////////////////////////////////////////////////////////////////////

#include "TLRS4413.h"

ClassImp( TLRS4413 )

const char fDescription[] = "LeCroy 4413 discriminator";  

/////////////////////////////////////////////////////////////////////
TLRS4413::TLRS4413(TCamacBranch *cbd, int c, int n): 
fCBD( cbd ), fCrate(c), fSlot(n)
{
  InitUnit();
}
/////////////////////////////////////////////////////////////////////
TLRS4413::TLRS4413(TCamacCrate *C, int n):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{
  InitUnit();
}
/////////////////////////////////////////////////////////////////////
TLRS4413::~TLRS4413( )
{
}
/////////////////////////////////////////////////////////////////////
void TLRS4413::InitUnit()
{
  if( ! fCBD->Online( fCrate ) )
    {
      printf("Camac module %s in crate %d slot %d: file %s line %d\n", 
	     fDescription, fCrate, fSlot, __FILE__, __LINE__);            
      fprintf( stderr, "Warning: Crate is not online\n");
    }

  pReadMask   = fCBD->cnaf( fCrate, fSlot, 0, 0);    
  pWriteMask  = fCBD->cnaf( fCrate, fSlot, 0, 16);    
  pReadThres  = fCBD->cnaf( fCrate, fSlot, 0, 1);    
  pWriteThres = fCBD->cnaf( fCrate, fSlot, 0, 17);    
  pLocal      = fCBD->cnaf( fCrate, fSlot, 0, 24);    
  pRemote     = fCBD->cnaf( fCrate, fSlot, 0, 26);      

  SetRemoteMode();
  EnableChannels( 0xffff);

  return;
}
/////////////////////////////////////////////////////////////////////
void TLRS4413::EnableChannels( UShort_t bp)
{
  fCBD->Write(pWriteMask,~bp);
  return;
}
/////////////////////////////////////////////////////////////////////
void TLRS4413::SetRemoteMode()
{
  fCBD->Read(pRemote);
  return;
}
/////////////////////////////////////////////////////////////////////
void TLRS4413::SetLocalMode()
{
  fCBD->Read(pLocal);
  return;
}
/////////////////////////////////////////////////////////////////////
void TLRS4413::PrintDisabled()
{
  int mask = fCBD->Read(pReadMask);
  printf("Disabled channels (mask register) 0x%x\n", mask);
  return;
}
/////////////////////////////////////////////////////////////////////
void TLRS4413::CopyLocalThres()
{
  fCBD->Write(pWriteThres,0x0400);
  return;
}
/////////////////////////////////////////////////////////////////////
void TLRS4413::SetVoltThresholds( Double_t val)
{
  if ( val > 1.023 || val < 0.0)
    {
      printf("Camac module %s in crate %d slot %d: file %s line %d\n", 
	     fDescription, fCrate, fSlot, __FILE__, __LINE__);            
      fprintf( stderr, "Warning: threshold out of range %f\n", val);
      return;
    }
  fCBD->Write(pWriteThres,(UShort_t)(val*1000));
  return;
}           
/////////////////////////////////////////////////////////////////////
void TLRS4413::PrintThresholds()
{
  printf("Thresholds at %d mV\n", fCBD->Read(pReadThres));  
  return;
}




