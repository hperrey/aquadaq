# -*- mode: cmake; -*-
# These macros generate config files with hard-coded values determined
# at config time to be included in C(++), or bash scripts
# 

# C/C++
CONFIGURE_FILE("${CMAKE_CURRENT_SOURCE_DIR}/cmake/configVariables.cmake.h" "${CMAKE_CURRENT_BINARY_DIR}/configVariables.h" @ONLY)
INCLUDE_DIRECTORIES("${CMAKE_CURRENT_BINARY_DIR}")
INSTALL(FILES ${CMAKE_CURRENT_BINARY_DIR}/configVariables.h
  DESTINATION script)

# bash
CONFIGURE_FILE("${CMAKE_CURRENT_SOURCE_DIR}/cmake/configVariables.cmake.sh" "${CMAKE_CURRENT_BINARY_DIR}/configVariables.sh" @ONLY)
INSTALL(FILES ${CMAKE_CURRENT_BINARY_DIR}/configVariables.sh
  DESTINATION script)

# make sure that this script is run every time (compilation only triggered though if file changes)
ADD_CUSTOM_TARGET(version ALL)
