///////////////////////////////////////////////////////////////////
//
// Caen 792 32 channel QDC 
//
// Magnus Lundin 2005-07-28 
// John Annand 19/02/14 Modify for TSIS1100 interface class
//
///////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "TCaen792.h"
#include "TSIS1100.h"

ClassImp( TCaen792 )

const char description[] = "Caen 792 QDC";
const UShort_t CAEN792SIZE = 0xffff;

///////////////////////////////////////////////////////////////////
TCaen792::TCaen792( TSIS1100* sis, UInt_t BASE, UShort_t debugflag)
{
  fSIS = sis;
  hwaddress = BASE;
  debug = debugflag;
  fbase = BASE;

  DataReset();
  SoftwareReset();

  DeactivateChannels(0);

  // Default is not to suppress under and overflows
  SuppressUnderflow( 0);
  SuppressOverflow( 0);

  SetIped( 180);
}
///////////////////////////////////////////////////////////////////
TCaen792::~TCaen792( )
{
}
///////////////////////////////////////////////////////////////////
// Generic write/read access to the 16 bit memory area (the control section)
void TCaen792::SetShort( UShort_t address, UShort_t value)
{
  fSIS->WA32D16(fbase+address,value);
  return;
}
UShort_t TCaen792::GetShort( UShort_t address)
{
  return fSIS->RA32D16(fbase+address);
}
///////////////////////////////////////////////////////////////////
void TCaen792::SuppressOverflow( Bool_t flag)
{
  if ( flag == 0) // dont suppress overflows
    {
      SetShort( 0x1032, 0x8);
    }
  else            // do suppress underflows
    {
      SetShort( 0x1034, 0x8);
    }
  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen792::SuppressUnderflow( Bool_t flag, UShort_t thres)
{  
  if ( flag == 0) // dont suppress underflows
    {
      SetUnderflowThresholds( 0);
      SetShort( 0x1032, 0x10);
    }
  else            // do suppress underflows
    {
      SetUnderflowThresholds( thres);
      SetShort( 0x1034, 0x10);
    }
  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen792::SetUnderflowThreshold( UShort_t channel, UShort_t thres)
{
  // Sets the threshold for a given channel. The kill bit is preserved.
  if ( channel > 31 )
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("channel > 31\n");
      exit(-1);
    }

  if ( thres > 0xfff )
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("thres out of range\n");
      exit(-1);
    }

  // enable the thresholds
  SetShort( 0x1034, 0x10);

  // Need to right shift the thres (section 2.4 and above)
  thres = thres >> 4;

  // See if the kill bit is set and set the threshold
  // while preserving the kill bit
  if ( GetShort( 0x1080 + channel*2) & 0x0100 )
    SetShort( 0x1080 + channel*2, thres + 0x100);      
  else
    SetShort( 0x1080 + channel*2, thres);      

  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen792::SetUnderflowThresholds( UShort_t thres)
{
  // Sets the same threshold for all 32 channels
  UShort_t i;

  // set thresholds
  for ( i = 0; i < 32; i++)
    SetUnderflowThreshold( i, thres);

  return;
}  
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen792::GetData( UShort_t *ch, UShort_t *val)
{
  UShort_t mult = 0, i;

  if ( debug > 1)
    printf("In GetData()\n");

  if ( Busy())
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Module busy, return\n");
      return 0;      
    }    

  if ( BufferEmpty())        
    {
      if ( debug > 1)
	{
	  printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
	  printf("Buffer is empty\n");
	}
      return 0;      
    }

  mult = GetMultiplicity();           /* get the multiplicity of the event */
  if ( debug > 1)
    printf("In GetData() mult is %d\n", mult);

  for ( i = 0; i < mult; i++)
    GetDatum( ch+i, val+i);           /* the current datum is copied ie channel and value */ 	    

  if ( ! IsEOB() )                    /* check that we have an eob */
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("No EOB found, exit\n");
      exit(-1);
    }

  if ( ! BufferEmpty() )              /* check that the buffer is empty */
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("There is still data in buffer, reseting data and continue\n");
      DataReset();
    }

  return mult;
}
/////////////////////////////////////////////////////////////////////////
void TCaen792::DeactivateChannels( UInt_t bitmask)
{
  // Set the kill bit (see 4.40) for selected channels. 
  // Channels with the kill bit set will not be converted.
  
  UShort_t i;
  UShort_t thres;   /* the previously set threshold */

  for ( i = 0; i < 32; i++)
    {
      thres = 0xff & GetShort( 0x1080 + i*2);
      if ( 0x1 & ((bitmask) >> i) )  
	SetShort( 0x1080 + i*2, thres + 0x100);  /* the kill bit */
      else
	SetShort( 0x1080 + i*2, thres);
    }
  return;
}
/////////////////////////////////////////////////////////////////////////
void TCaen792::PrintIped()
{
  UShort_t iped = 0x00ff & GetShort( 0x1060);
  printf("iped 0x%x (hex) %d (dec)\n", iped, iped);
  return;
}
/////////////////////////////////////////////////////////////////////////
void TCaen792::SetIped( UShort_t iped)
{
  if ( iped < 60)
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Its recommended to use iped >= 60, but Im sure you know what you are doing\n");
    }
  if ( iped > 255)
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Cant set iped above 255, iped set at 255\n");
      iped = 255;
    }
  SetShort( 0x1060, iped);

  return;
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen792::GetMultiplicity()
{
  UInt_t header;

  // Get the value at the read pointer
  header = fSIS->RA32D32(fbase);

  if ( (0x7 & (header >> 24)) != 0x2 )
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      perror("Not a header, exit");
      exit(-1);
    }
  else
    return (0x3f & (header >> 8));
}
//////////////////////////////////////////////////////////////////////////
void TCaen792::GetDatum( UShort_t *channel, UShort_t *value)
{
  UInt_t datum;

  // Get the value at the read pointer
  datum = fSIS->RA32D32(fbase);

  if ( (0x7 & (datum >> 24)) != 0x0 )   // a datum?
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      perror("Not a datum, exit");
      exit(-1);
    }
  else
    {
      *channel = 0x1f & (datum >> 16);
      *value   = 0x3fff & datum; 
    }
  return;
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen792::IsEOB()
{
  UInt_t eob;

  // Get the value at the read pointer
  eob = fSIS->RA32D32(fbase);

  if ( (0x7 & (eob >> 24)) != 0x4 )
    return 0;
  else
    return 1;
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen792::BufferEmpty()
{
  if ( (GetShort(0x1022) >> 1) & 0x1 )
    return 1;
  else
    return 0;
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen792::Busy()
{
  if ( (GetShort(0x100e) >> 2) & 0x1 )
    return 1;
  else
    return 0;
}
//////////////////////////////////////////////////////////////////////////
// Print the current thresholds and kill bits to screen
void TCaen792::PrintThresholds()
{
  UShort_t i, val;

  fprintf( stderr, "----------- Thresholds -----------\n");

  for ( i = 0; i < 32; i++)
    {
      val = 0x1ff & GetShort( 0x1080 + i*2);
      fprintf( stderr, "Ch. %d  : 0x%x  : kill %x \n", i, 0xfff & (val << 4), 0x1 & (val >> 8));
    }
  return;
}
//////////////////////////////////////////////////////////////////////////
// Print some info to screen
void TCaen792::Info()
{
  printf("firmware : 0x%x\n", GetShort( 0x1000));  
  
  printf("geo : 0x%x\n", 0x001f & GetShort( 0x1002));  

  printf("interrupt level: 0x%x\n", 0x0007 & GetShort(0x100a));  

  printf("interrupt vector: 0x%x\n", 0x00ff & GetShort(0x100c));  

  printf("event trigger: 0x%x\n", 0x001f & GetShort(0x1020));  

  printf("event counter low: 0x%x\n", GetShort(0x1024));  

  printf("event counter high: 0x%x\n", 0x00ff & GetShort(0x1026));  

  printf("Board id : %d \n", ((0x00ff & GetShort(0x803a)) << 8) + (0x00ff & GetShort(0x803e)));

  return;
}
//////////////////////////////////////////////////////////////////////////
// Print some status to screen
void TCaen792::PrintStatus()
{
  unsigned short stat1,stat2,bitset1,bitset2;

  stat1 = GetShort( 0x100e);
  printf("stat1 %x\n", stat1);

  if ( stat1 & 0x0001 )
    printf("stat 1 \t: DREADY\n");
  else
    printf("stat 1 \t: NO DREADY\n");

  if ( stat1 & (0x0001 << 1) )
    printf("stat 1 \t: GLOBAL DREADY\n");
  else
    printf("stat 1 \t: NO GLOBAL DREADY\n");

  if ( stat1 & (0x0001 << 2) )
    printf("stat 1 \t: BUSY\n");
  else
    printf("stat 1 \t: NOT BUSY\n");

  if ( stat1 & (0x0001 << 3) )
    printf("stat 1 \t: GLOBAL BUSY\n");
  else
    printf("stat 1 \t: NOT GLOBAL BUSY\n");

  if ( stat1 & (0x0001 << 4) )
    printf("stat 1 \t: AMNESIA\n");
  else
    printf("stat 1 \t: NO AMNESIA\n");

  if ( stat1 & (0x0001 << 5) )
    printf("stat 1 \t: PURGED\n");
  else
    printf("stat 1 \t: NOT PURGED\n");

  if ( stat1 & (0x0001 << 6) )
    printf("stat 1 \t: ALL TERM ON\n");
  else
    printf("stat 1 \t: NOT ALL TERM ON\n");

  if ( stat1 & (0x0001 << 7) )
    printf("stat 1 \t: ALL TERM OFF\n");
  else
    printf("stat 1 \t: NOT ALL TERM OFF\n");

  if ( stat1 & (0x0001 << 8) )
    printf("stat 1 \t: EVRDY\n");
  else
    printf("stat 1 \t: NOT EVRDY\n");


  stat2 = GetShort( 0x1022);
  printf("stat2 %x\n", stat2);

  if ( stat2 & (0x0001 << 1) )
    printf("stat 2 \t: BUFFER EMPTY\n");
  else
    printf("stat 2 \t: BUFFER NOT EMPTY\n");
  if ( stat2 & (0x0001 << 2) )
    printf("stat 2 \t: BUFFER FULL\n");
  else
    printf("stat 2 \t: BUFFER NOT FULL\n");

  if ( stat2 & (0x0001 << 4) )
    printf("stat 2 \t: DSEL0 1\n");
  else
    printf("stat 2 \t: DSEL0 0\n");

  if ( stat2 & (0x0001 << 5) )
    printf("stat 2 \t: DSEL1 1\n");
  else
    printf("stat 2 \t: DSEL1 0\n");

  if ( stat2 & (0x0001 << 6) )
    printf("stat 2 \t: CSEL0 1\n");
  else
    printf("stat 2 \t: CSEL0 0\n");

  if ( stat2 & (0x0001 << 7) )
    printf("stat 2 \t: CSEL1 1\n");
  else
    printf("stat 2 \t: CSEL1 0\n");


  bitset1 = GetShort( 0x1006);
  printf("bitset1 %x\n", bitset1);

  if ( bitset1 & (0x0001 << 3) )
    printf("bset 1 \t: BERR\n");
  else
    printf("bset 1 \t: NO BERR\n");

  if ( bitset1 & (0x0001 << 4) )
    printf("bset 1\t: SELECT ADDRESS ON (ADER)\n");
  else
    printf("bset 1\t: SELECT ADDRESS OFF (ROTARY SW)\n");

  if ( bitset1 & (0x0001 << 7) )
    printf("bset 1\t: SOFTWARE RESET\n");
  else
    printf("bset 1\t: NO SOFTWARE RESET\n");


  bitset2 = GetShort( 0x1032);
  printf("bitset2 %x\n", bitset2);

  if ( bitset2 & (0x0001 << 0) )
    printf("bset 2 \t: MEM TEST\n");
  else
    printf("bset 2 \t: NO MEM TEST\n");

  if ( bitset2 & (0x0001 << 1) )
    printf("bset 2 \t: OFFLINE\n");
  else
    printf("bset 2 \t: NOT OFFLINE\n");

  if ( bitset2 & (0x0001 << 2) )
    printf("bset 2 \t: CLEAR DATA\n");
  else
    printf("bset 2 \t: NOT CLEAR DATA\n");

  if ( bitset2 & (0x0001 << 3) )
    printf("bset 2 \t: OVER RANGE CHECK DISABLED\n");
  else
    printf("bset 2 \t: OVER RANGE CHECK ENABLED\n");

  if ( bitset2 & (0x0001 << 4) )
    printf("bset 2 \t: LOW THRESHOLD CHECK DISABLED\n");
  else
    printf("bset 2 \t: LOW THRESHOLD CHECK ENABLED\n");

  if ( bitset2 & (0x0001 << 6) )
    printf("bset 2 \t: TEST ACQ ENABLED\n");
  else
    printf("bset 2 \t: TEST ACQ DISABLED\n");

  if ( bitset2 & (0x0001 << 7) )
    printf("bset 2 \t: SLIDE ENABLED\n");
  else
    printf("bset 2 \t: SLIDE DISABLED\n");

  if ( bitset2 & (0x0001 << 8) )                        // section 2.3
    printf("bset 2 \t: SMALL THRESHOLD STEPS\n");
  else
    printf("bset 2 \t: NORMAL THRESHOLD STEPS\n");

  if ( bitset2 & (0x0001 << 11) )
    printf("bset 2 \t: AUTO INCR ENABLED\n");
  else
    printf("bset 2 \t: AUTO INC DISABLED\n");

  if ( bitset2 & (0x0001 << 12) )
    printf("bset 2 \t: EMPTY PROG ENABLED\n");
  else
    printf("bset 2 \t: EMPTY PROG DISABLED\n");

  if ( bitset2 & (0x0001 << 13) )
    printf("bset 2 \t: SLIDE_SUB DISABLED\n");
  else
    printf("bset 2 \t: SLIDE_SUB ENABLED\n");

  if ( bitset2 & (0x0001 << 14) )
    printf("bset 2 \t: ALL TRG ENABLED\n");
  else
    printf("bset 2 \t: ALL TRG DISABLED\n");

  return;
}
/////////////////////////////////////////////////////////////////////////
