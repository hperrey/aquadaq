
#include "TBorer1008.h"

ClassImp( TBorer1008 )

TBorer1008::TBorer1008(TCamacBranch *cbd, int c, int n): 
fCBD( cbd ), fCrate(c), fSlot(n)
{
  InitUnit();
}

TBorer1008::TBorer1008(TCamacCrate *C, int n):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{
  InitUnit();
}

TBorer1008::~TBorer1008( )
{
}

void TBorer1008::InitUnit()
{
  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  for( int i = 0; i < fNoChannels; i++)
    {
      // Note t = 0 to indicate 24-bit access
      pRead[i] = (volatile Int_t *)fCBD->cnaf( fCrate, fSlot, i, 0, 0);
      pWrite[i] = (volatile Int_t *)fCBD->cnaf( fCrate, fSlot, i, 17, 0);
      pStart[i] = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, i, 25);
      pClear[i] = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, i, 9);
    }
}

Int_t TBorer1008::ReadArray( Int_t *Array, Int_t count)
{
  // Read an array of count values into Array
  // from channel starting on 0.
  
  int i;

  for( i = 0; i < count; i++)
    *Array++ = *(Int_t *)pRead[i];

  return i;
}

void TBorer1008::Test(int l)
{

}

void TBorer1008::Print( )
{
  for( int i = 0; i < fNoChannels; i++)
    printf( "Channel: %d, value: %d\n", i, operator[](i));
}
