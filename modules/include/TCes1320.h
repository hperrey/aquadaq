/////////////////////////////////////////////////////
// 
// Ces 1320 camac output register
//
// Magnus Lundin 2005-10-26
//
////////////////////////////////////////////////////
#ifndef _TCes1320_H_
#define _TCes1320_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

class TCes1320: public TObject{

private:

  TCamacBranch *fCBD;
  const int fCrate;             // Crate number
  const int fSlot;              // Slot number

  volatile UShort_t *pRead;
  volatile UShort_t *pClear;
  volatile UShort_t *pSetBits;
  volatile UShort_t *pUnSetBits;
  volatile UShort_t *pDisablePulse;
  volatile UShort_t *pEnablePulse;
  
public:
  TCes1320(TCamacBranch *cbd, int c, int n);
  TCes1320(TCamacCrate *C, int n);
  virtual ~TCes1320();

  UShort_t Read();
  void Clear();
  Bool_t SetBits( UShort_t value);
  Bool_t UnSetBits( UShort_t value);
  void ToggleBits( UShort_t value);
  void DisablePulse(){ *pDisablePulse;}
  void EnablePulse(){ *pEnablePulse;}

  Bool_t Q() {return fCBD->Q();} // Q response
  Bool_t X() {return fCBD->X();} // X response

  void Print();
  
  ClassDef( TCes1320, 0)  // Camac unit
};

#endif /* _TCes1320_H_ */
